﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataLayer
{
    public class DataAccess: IDisposable
    {
        public DataAccess()
        {

        }
             
        public JudoModelContainer JudoModelContainerDbContext
        {
            get
            {
                var dbContext = new JudoModelContainer();

                dbContext.Configuration.AutoDetectChangesEnabled = true;
                dbContext.Configuration.LazyLoadingEnabled = true;
                dbContext.Configuration.ProxyCreationEnabled = true;
                dbContext.Configuration.ValidateOnSaveEnabled = false;
  
                return dbContext;
            }
        }

        #region IDisposable Members

        // Track whether Dispose has been called. 
        private bool disposed = false;

        /// <summary>
        /// Disposes manager private resources. 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method. 
            // Therefore, you should call GC.SupressFinalize to 
            // take this object off the finalization queue 
            // and prevent finalization code for this object 
            // from executing a second time.
            GC.SuppressFinalize(this);

        }

        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.

                    DisposeData();

                    // Note disposing has been done.
                    disposed = true;
                }
            }
        }

        private void DisposeData()
        {
            using (var dbContext = JudoModelContainerDbContext)
            {
                dbContext.Database.Connection.Close();
            }
        }


        #endregion
    }
}
