
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/26/2017 01:10:56
-- Generated from EDMX file: D:\Utenti\Adriano\Documenti\Visual Studio 2015\Projects\Judo\Judo\Judo.DataLayer\JudoModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TorneoJudoDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AtletaSocieta]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AtletaSet] DROP CONSTRAINT [FK_AtletaSocieta];
GO
IF OBJECT_ID(N'[dbo].[FK_ReportAtleta]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ReportSet] DROP CONSTRAINT [FK_ReportAtleta];
GO
IF OBJECT_ID(N'[dbo].[FK_ReportCategoria]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ReportSet] DROP CONSTRAINT [FK_ReportCategoria];
GO
IF OBJECT_ID(N'[dbo].[FK_AtletaCategoria_Atleta]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AtletaCategoria] DROP CONSTRAINT [FK_AtletaCategoria_Atleta];
GO
IF OBJECT_ID(N'[dbo].[FK_AtletaCategoria_Categoria]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AtletaCategoria] DROP CONSTRAINT [FK_AtletaCategoria_Categoria];
GO
IF OBJECT_ID(N'[dbo].[FK_TrofeoConfigCategoria]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CategoriaSet] DROP CONSTRAINT [FK_TrofeoConfigCategoria];
GO
IF OBJECT_ID(N'[dbo].[FK_TrofeoConfigSocieta]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SocietaSet] DROP CONSTRAINT [FK_TrofeoConfigSocieta];
GO
IF OBJECT_ID(N'[dbo].[FK_TrofeoConfigClassificaCategoria]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClassificaCategoriaSet] DROP CONSTRAINT [FK_TrofeoConfigClassificaCategoria];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ReportSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ReportSet];
GO
IF OBJECT_ID(N'[dbo].[CategoriaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CategoriaSet];
GO
IF OBJECT_ID(N'[dbo].[AtletaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AtletaSet];
GO
IF OBJECT_ID(N'[dbo].[SocietaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SocietaSet];
GO
IF OBJECT_ID(N'[dbo].[CombattimentoSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CombattimentoSet];
GO
IF OBJECT_ID(N'[dbo].[TrofeoConfigSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TrofeoConfigSet];
GO
IF OBJECT_ID(N'[dbo].[ClassificaCategoriaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClassificaCategoriaSet];
GO
IF OBJECT_ID(N'[dbo].[AtletaCategoria]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AtletaCategoria];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ReportSet'
CREATE TABLE [dbo].[ReportSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [Stato] int  NOT NULL,
    [CodicePosizioneAtleta] nvarchar(max)  NOT NULL,
    [TipologiaGara] int  NOT NULL,
    [NumeroLivelliGara] int  NOT NULL,
    [CodiceUnivoco] nvarchar(max)  NOT NULL,
    [CategoriaId] int  NULL,
    [TrofeoGuid] uniqueidentifier  NOT NULL,
    [Atleta_Id] int  NULL
);
GO

-- Creating table 'CategoriaSet'
CREATE TABLE [dbo].[CategoriaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [Codice] nvarchar(max)  NOT NULL,
    [Sesso] nvarchar(max)  NOT NULL,
    [Peso] nvarchar(max)  NOT NULL,
    [Eta] nvarchar(max)  NOT NULL,
    [NumeroTatami] int  NOT NULL,
    [DurataGara] int  NOT NULL,
    [DurataExtraGara] int  NOT NULL,
    [Stato] int  NOT NULL,
    [Descrizione] nvarchar(max)  NOT NULL,
    [TipologiaGara] int  NOT NULL,
    [TrofeoConfigId] int  NOT NULL,
    [TrofeoGuid] uniqueidentifier  NOT NULL,
    [Trofeo_Id] int  NOT NULL
);
GO

-- Creating table 'AtletaSet'
CREATE TABLE [dbo].[AtletaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Cognome] nvarchar(max)  NOT NULL,
    [Anno] nvarchar(max)  NOT NULL,
    [Sesso] int  NOT NULL,
    [Ranking] int  NOT NULL,
    [Codice] nvarchar(max)  NOT NULL,
    [SocietaId] int  NOT NULL,
    [CategoriaId] int  NOT NULL,
    [TrofeoGuid] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'SocietaSet'
CREATE TABLE [dbo].[SocietaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Nazione] nvarchar(max)  NOT NULL,
    [PunteggioTotale] int  NOT NULL,
    [Classifica] int  NOT NULL,
    [TrofeoConfigId] int  NOT NULL,
    [TrofeoGuid] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'CombattimentoSet'
CREATE TABLE [dbo].[CombattimentoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [NumeroTatami] nvarchar(max)  NOT NULL,
    [Vincente] nvarchar(max)  NOT NULL,
    [Stato] int  NOT NULL,
    [IdReportAtletaA] nvarchar(max)  NOT NULL,
    [IdReportAtletaB] nvarchar(max)  NOT NULL,
    [AtletaA] nvarchar(max)  NOT NULL,
    [AtletaB] nvarchar(max)  NOT NULL,
    [CodicePosizioneAtletaA] nvarchar(max)  NOT NULL,
    [CodicePosizioneAtletaB] nvarchar(max)  NOT NULL,
    [DataCombattimento] nvarchar(max)  NOT NULL,
    [TrofeoGuid] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'TrofeoConfigSet'
CREATE TABLE [dbo].[TrofeoConfigSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [Titolo] nvarchar(max)  NOT NULL,
    [DataEventoStr] nvarchar(max)  NOT NULL,
    [DataEvento] datetime  NOT NULL,
    [TempoImNO] int  NOT NULL,
    [TempoYuko] int  NOT NULL,
    [TempoWari] int  NOT NULL,
    [TempoIppon] int  NOT NULL,
    [NumeroAtletiPartecipanti] int  NOT NULL,
    [NumeroCategoriePartecipanti] int  NOT NULL,
    [NumeroSocietaPartecipanti] int  NOT NULL,
    [ClassificaCategoriaId] int  NOT NULL
);
GO

-- Creating table 'ClassificaCategoriaSet'
CREATE TABLE [dbo].[ClassificaCategoriaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IdGuid] uniqueidentifier  NOT NULL,
    [Posizione] int  NOT NULL,
    [CategoriaId] uniqueidentifier  NOT NULL,
    [AtletaId] uniqueidentifier  NOT NULL,
    [TrofeoConfigId] int  NOT NULL,
    [TrofeoGuid] uniqueidentifier  NOT NULL
);
GO

-- Creating table 'AtletaCategoria'
CREATE TABLE [dbo].[AtletaCategoria] (
    [ElencoAtleti_Id] int  NOT NULL,
    [ElencoCategorie_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ReportSet'
ALTER TABLE [dbo].[ReportSet]
ADD CONSTRAINT [PK_ReportSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CategoriaSet'
ALTER TABLE [dbo].[CategoriaSet]
ADD CONSTRAINT [PK_CategoriaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AtletaSet'
ALTER TABLE [dbo].[AtletaSet]
ADD CONSTRAINT [PK_AtletaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SocietaSet'
ALTER TABLE [dbo].[SocietaSet]
ADD CONSTRAINT [PK_SocietaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CombattimentoSet'
ALTER TABLE [dbo].[CombattimentoSet]
ADD CONSTRAINT [PK_CombattimentoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TrofeoConfigSet'
ALTER TABLE [dbo].[TrofeoConfigSet]
ADD CONSTRAINT [PK_TrofeoConfigSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClassificaCategoriaSet'
ALTER TABLE [dbo].[ClassificaCategoriaSet]
ADD CONSTRAINT [PK_ClassificaCategoriaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ElencoAtleti_Id], [ElencoCategorie_Id] in table 'AtletaCategoria'
ALTER TABLE [dbo].[AtletaCategoria]
ADD CONSTRAINT [PK_AtletaCategoria]
    PRIMARY KEY CLUSTERED ([ElencoAtleti_Id], [ElencoCategorie_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [SocietaId] in table 'AtletaSet'
ALTER TABLE [dbo].[AtletaSet]
ADD CONSTRAINT [FK_AtletaSocieta]
    FOREIGN KEY ([SocietaId])
    REFERENCES [dbo].[SocietaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AtletaSocieta'
CREATE INDEX [IX_FK_AtletaSocieta]
ON [dbo].[AtletaSet]
    ([SocietaId]);
GO

-- Creating foreign key on [Atleta_Id] in table 'ReportSet'
ALTER TABLE [dbo].[ReportSet]
ADD CONSTRAINT [FK_ReportAtleta]
    FOREIGN KEY ([Atleta_Id])
    REFERENCES [dbo].[AtletaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReportAtleta'
CREATE INDEX [IX_FK_ReportAtleta]
ON [dbo].[ReportSet]
    ([Atleta_Id]);
GO

-- Creating foreign key on [CategoriaId] in table 'ReportSet'
ALTER TABLE [dbo].[ReportSet]
ADD CONSTRAINT [FK_ReportCategoria]
    FOREIGN KEY ([CategoriaId])
    REFERENCES [dbo].[CategoriaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReportCategoria'
CREATE INDEX [IX_FK_ReportCategoria]
ON [dbo].[ReportSet]
    ([CategoriaId]);
GO

-- Creating foreign key on [ElencoAtleti_Id] in table 'AtletaCategoria'
ALTER TABLE [dbo].[AtletaCategoria]
ADD CONSTRAINT [FK_AtletaCategoria_Atleta]
    FOREIGN KEY ([ElencoAtleti_Id])
    REFERENCES [dbo].[AtletaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ElencoCategorie_Id] in table 'AtletaCategoria'
ALTER TABLE [dbo].[AtletaCategoria]
ADD CONSTRAINT [FK_AtletaCategoria_Categoria]
    FOREIGN KEY ([ElencoCategorie_Id])
    REFERENCES [dbo].[CategoriaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AtletaCategoria_Categoria'
CREATE INDEX [IX_FK_AtletaCategoria_Categoria]
ON [dbo].[AtletaCategoria]
    ([ElencoCategorie_Id]);
GO

-- Creating foreign key on [Trofeo_Id] in table 'CategoriaSet'
ALTER TABLE [dbo].[CategoriaSet]
ADD CONSTRAINT [FK_TrofeoConfigCategoria]
    FOREIGN KEY ([Trofeo_Id])
    REFERENCES [dbo].[TrofeoConfigSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TrofeoConfigCategoria'
CREATE INDEX [IX_FK_TrofeoConfigCategoria]
ON [dbo].[CategoriaSet]
    ([Trofeo_Id]);
GO

-- Creating foreign key on [TrofeoConfigId] in table 'SocietaSet'
ALTER TABLE [dbo].[SocietaSet]
ADD CONSTRAINT [FK_TrofeoConfigSocieta]
    FOREIGN KEY ([TrofeoConfigId])
    REFERENCES [dbo].[TrofeoConfigSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TrofeoConfigSocieta'
CREATE INDEX [IX_FK_TrofeoConfigSocieta]
ON [dbo].[SocietaSet]
    ([TrofeoConfigId]);
GO

-- Creating foreign key on [TrofeoConfigId] in table 'ClassificaCategoriaSet'
ALTER TABLE [dbo].[ClassificaCategoriaSet]
ADD CONSTRAINT [FK_TrofeoConfigClassificaCategoria]
    FOREIGN KEY ([TrofeoConfigId])
    REFERENCES [dbo].[TrofeoConfigSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TrofeoConfigClassificaCategoria'
CREATE INDEX [IX_FK_TrofeoConfigClassificaCategoria]
ON [dbo].[ClassificaCategoriaSet]
    ([TrofeoConfigId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------