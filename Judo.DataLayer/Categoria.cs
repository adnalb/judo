//------------------------------------------------------------------------------
// <auto-generated>
//     Codice generato da un modello.
//
//     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
//     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Judo.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class Categoria
    {
        public Categoria()
        {
            this.ElencoReports = new HashSet<Report>();
            this.ElencoAtleti = new HashSet<Atleta>();
        }
    
        public int Id { get; set; }
        public System.Guid IdGuid { get; set; }
        public string Codice { get; set; }
        public string Sesso { get; set; }
        public string Peso { get; set; }
        public string Eta { get; set; }
        public int NumeroTatami { get; set; }
        public int DurataGara { get; set; }
        public int DurataExtraGara { get; set; }
        public StatoCombattimento Stato { get; set; }
        public string Descrizione { get; set; }
        public TipologiaGara TipologiaGara { get; set; }
        public int TrofeoConfigId { get; set; }
        public System.Guid TrofeoGuid { get; set; }
    
        public virtual ICollection<Report> ElencoReports { get; set; }
        public virtual ICollection<Atleta> ElencoAtleti { get; set; }
        public virtual TrofeoConfig Trofeo { get; set; }
    }
}
