﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JudoVisualBasicToNetDBReader
{
    public class LogItem
    {
        public LogItem(string msg, bool isError = false)
        {
            this.Msg = msg;
            this.IsError = isError;
        }

        public string Msg { get; set; }
        public bool IsError { get; set; }
    }
}
