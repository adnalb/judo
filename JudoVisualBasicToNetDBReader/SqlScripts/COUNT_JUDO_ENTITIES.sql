USE TorneoJudoDB
GO

/****** Script for SelectTopNRows command from SSMS  ******/
SELECT Count(*) AS TROFEI FROM [TorneoJudoDB].[dbo].[TrofeoConfigSet]
SELECT Count(*) AS CATEGORIE FROM [TorneoJudoDB].[dbo].[CategoriaSet]
SELECT Count(*) AS SOCIETA FROM [TorneoJudoDB].[dbo].[SocietaSet]
SELECT Count(*) AS ATLETI FROM [TorneoJudoDB].[dbo].[AtletaSet]
SELECT Count(*) AS REPORTS FROM [TorneoJudoDB].[dbo].[ReportSet]
SELECT Count(*) AS CLASSIFICHE FROM [TorneoJudoDB].[dbo].[ClassificaCategoriaSet]
GO