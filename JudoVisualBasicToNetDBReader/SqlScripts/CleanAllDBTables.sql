﻿--USE TorneoJudoDB
--GO

delete from dbo.AtletaCategoria;
delete from dbo.ClassificaCategoriaSet;
delete from dbo.ReportSet;
delete from dbo.CategoriaSet;
delete from dbo.AtletaSet;
delete from dbo.SocietaSet;
delete from dbo.CombattimentoSet;
delete from dbo.TrofeoConfigSet;
--GO