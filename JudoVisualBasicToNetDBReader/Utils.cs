﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JudoVisualBasicToNetDBReader
{
    public static class Utils
    {
        /// <summary>
        /// Get FULL Exception stack trace (add InnerExceptions trace)
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string GetFullExceptionTrace(Exception ex)
        {
            if (ex != null)
            {
                try
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendFormat("Exception=[{0}][{1}]", ex.GetType(), ex.Message);

                    Exception innerEx = ex.InnerException;

                    while (innerEx != null)
                    {
                        sb.AppendFormat(" (InnerException=[{0}][{1}])", innerEx.GetType(), innerEx.Message);

                        innerEx = innerEx.InnerException;
                    }

                    return sb.ToString();
                }
                catch (Exception exx)
                {
                    System.Diagnostics.Trace.TraceError("GetFullExceptionTrace exception: {0}", exx);
                }

                return string.Format("{0}", ex);
            }

            return string.Empty;
        }
    }
}
