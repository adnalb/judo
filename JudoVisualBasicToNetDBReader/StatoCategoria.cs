﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Judo.DataModel;

namespace JudoVisualBasicToNetDBReader
{
    public class StatoCategoria
    {
        public Guid IdGuid { get; set; }
        public string Codice { get; set; }
        public Enums.StatoCombattimento Stato { get; set; }
        public Enums.TipologiaGara TipoGara { get; set; }

        public Guid TrofeoGuid { get; set; }

        public int LivelloReportPrincipale{ get; set; }
        public int LivelloReportRecuperi { get; set; }
    }
}
