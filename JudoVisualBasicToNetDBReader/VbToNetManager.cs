﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Data.OleDb;
using Judo.DataModel;
using Judo.DataLayer;

namespace JudoVisualBasicToNetDBReader
{
    public class VbToNetManager
    {
        #region Variables

        public event EventHandler<string> LogMsg;
        public event EventHandler<string> LogError;

        private Thread readerThread = null;
        private bool doReading = false;

        private Thread logThread = null;
        private bool doLogReading = false;

        private Queue<LogItem> logQueue = null;

        private Dictionary<Guid, AtletaInfo> ElencoAtleti = null;
        private Dictionary<Guid, CategoriaInfo> ElencoCategorie = null;
        private Dictionary<Guid, ClassificaAtletaCategoriaInfo> ElencoClassificheCategorie = null;
        private Dictionary<Guid, ReportInfo> ElencoReports = null;
        private Dictionary<Guid, SocietaInfo> ElencoSocieta = null;
        private Dictionary<Guid, StatoCategoria> StatoGestioneCategorie = null;

        private readonly TrofeoInfo infoTrofeo = null;

        private DataAccess dataAccess = null;

        #endregion

        #region Constructor

        public VbToNetManager(string vbDbPath = null)
        {
            VbDBFullPath = vbDbPath;

            if (string.IsNullOrEmpty(VbDBFullPath))
            {
                VbDBFullPath = Properties.Settings.Default.VbDBFullPath;
            }

            readerThread = null;
            doReading = false;

            logThread = null;
            doLogReading = false;

            logQueue = new Queue<LogItem>();

            infoTrofeo = new TrofeoInfo();

            ElencoAtleti = new Dictionary<Guid, AtletaInfo>();
            ElencoCategorie = new Dictionary<Guid, CategoriaInfo>();
            ElencoClassificheCategorie = new Dictionary<Guid, ClassificaAtletaCategoriaInfo>();
            ElencoReports = new Dictionary<Guid, ReportInfo>();
            ElencoSocieta = new Dictionary<Guid, SocietaInfo>();
            StatoGestioneCategorie = new Dictionary<Guid, StatoCategoria>();

            dataAccess = new DataAccess();

        }

        #endregion

        #region Properties

        public string VbDBFullPath { get; set; }

        public string VbDBConnectionString
        {
            get
            {
                // "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="D:\Projects\Visual Studio 2013\Judo\Judo\JudoVisualBasicToNetDBReader\Data\Tabelloni.mdb";Persist Security Info=True"

                return string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Persist Security Info=True;", VbDBFullPath);
            }
        }

        public DataAccess DataAccessContext
        {
            get
            {
                return dataAccess;
            }
        }

        public TrofeoInfo InfoTrofeo 
        {
            get
            {
                return infoTrofeo;
            }
        }

        #endregion

        #region Public Methods

        public void StartVbDataReading()
        {
            StartLogThread();

            ElencoAtleti.Clear();    
            ElencoCategorie.Clear();
            ElencoClassificheCategorie.Clear();
            ElencoReports.Clear();
            ElencoSocieta.Clear();
            StatoGestioneCategorie.Clear();

            if (string.IsNullOrEmpty(VbDBFullPath))
            {
                throw new ArgumentNullException("VbDBFullPath", "Percorso del database VisualBasic non impostato!");
            }

            if (StartReaderThread() == false)
            {
                throw new Exception("Errore nello Start gestore lettura dati database");
            }

        }
        
        public void StopVbDataReading()
        {
            if(StopReaderThread() == false)
            {
                throw new Exception("Errore nello Stop gestore lettura dati database");
            }

            StopLogThread();
        }

        #endregion

        #region Private Methods

        #region Log

        private bool StartLogThread()
        {
            StopLogThread();

            try
            {
                if (logThread == null)
                {
                    logThread = new Thread(new ThreadStart(LogThreadLoop));
                    logThread.IsBackground = true;
                    logThread.Name = "JudoVbToNetLogThread";

                    logThread.Start();

                }

                return true;
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("StartLogThread error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error, true);
            }
            return false;
        }

        private bool StopLogThread()
        {
            try
            {
                doLogReading = false;

                if (logThread != null)
                {
                    logThread.Abort();
                }

                doLogReading = false;
                logThread = null;

                return true;
            }
            catch (ThreadAbortException)
            {
                doLogReading = false;
            }
            catch (Exception ex)
            {
                string error = string.Format("StopLogThread error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error, true);
            }
            return false;
        }

        private void LogThreadLoop()
        {
            try
            {
                logQueue.Clear();

                doLogReading = true;

                while (doLogReading)
                {
                    var logItem = logQueue.Dequeue();
                    if(logItem != null)
                    {
                        if(logItem.IsError)
                        {
                            OnLogError(logItem.Msg, true);
                        }
                        else
                        {
                            OnLogMsg(logItem.Msg, true);
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }

                }
            }
            catch (ThreadAbortException) {
                doLogReading = false;
            }
            catch (Exception ex)
            {
                doLogReading = false;
                logThread = null;

                string error = string.Format("LogThreadLoop error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error, true);
            }
        }

        private void OnLogMsg(string msg, bool immediateInvoke = true)
        {
            if (immediateInvoke)
            {
                LogMsg?.Invoke(this, msg);
                Thread.Sleep(Properties.Settings.Default.LogSleepInterval);
            }
            else
            {
                var logItem = new LogItem(msg, false);
                logQueue.Enqueue(logItem);
            }
        }

        private void OnLogError(string msg, bool immediateInvoke = true)
        {
            if (immediateInvoke)
            {
                LogError?.Invoke(this, msg);
                Thread.Sleep(Properties.Settings.Default.LogSleepInterval);
            }
            else
            {
                var logItem = new LogItem(msg, true);
                logQueue.Enqueue(logItem);
            }
        }

        #endregion

        #region MAIN LOOP METHODS

        private bool StartReaderThread()
        {
            StopReaderThread();

            try
            {
                if (readerThread == null)
                {
                    readerThread = new Thread(new ThreadStart(ReaderThreadLoop));
                    readerThread.IsBackground = true;
                    readerThread.Name = "JudoVbToNetReaderThread";

                    ResetJudoDB();

                    readerThread.Start();

                }

                return true;
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("StartReaderThread error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
            return false;
        }

        private bool StopReaderThread()
        {
            try
            {
                doReading = false;

                if (readerThread != null)
                {
                    readerThread.Abort();
                }

                doReading = false;
                readerThread = null;

                return true;
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("StopReaderThread error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
            return false;
        }

        private void ReaderThreadLoop()
        {
            try
            {
                doReading = true;

                while(doReading)
                {
                    ReadVbDBData();

                    WriteAllData();

                    TraceReadData();

                    TraceWriteData();

                    Thread.Sleep(Properties.Settings.Default.VbDBRefreshTimeout);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                doReading = false;
                readerThread = null;

                string error = string.Format("ReaderThreadLoop error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ResetJudoDB()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    if (dbContext.Database.Exists() == false)
                    {
                        dbContext.Database.Create();
                    }

                    if (Properties.Settings.Default.CleanDBOnStart == false) return;

                    dbContext.Database.ExecuteSqlCommand(Properties.Resources.CleanAllDBTables);

                    dbContext.SaveChanges();
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ResetJudoDB error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
                throw new InvalidOperationException("ResetJudoDB FAILURE!!");
            }
        }

        #endregion

        #region LETTURA DATI 

        private readonly int ReadSleepInterval = Properties.Settings.Default.ReadSleepInterval;

        private void ReadVbDBData()
        {
            try
            {
                string msg = "Processo lettura iniziato";
                OnLogMsg(msg);

                using (OleDbConnection connection = new OleDbConnection(VbDBConnectionString))
                {
                    connection.Open();

                    ReadConfig(connection);

                    ReadAllData(connection);

                    UpdateConfig();

                    TraceReadData();

                    msg = "Processo lettura terminato correttamente";
                    OnLogMsg(msg);
                }

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadVbDBData error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadConfig(OleDbConnection connection)
        {
            try
            {
                OnLogMsg("Recupero informazioni Trofeo");

                string queryString = "SELECT * FROM Config;";

                // Create the Command and Parameter objects.
                OleDbCommand command = new OleDbCommand(queryString, connection);

                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    infoTrofeo.Id = 0;

                    if(infoTrofeo.IdGuid == Guid.Empty)
                    {
                        infoTrofeo.IdGuid = Guid.NewGuid();
                    }

                    infoTrofeo.Titolo = Convert.ToString(reader["NGara"]).ToString();
                    infoTrofeo.DataEventoStr = Convert.ToString(reader["DataG"]);
                    infoTrofeo.DataEvento = GetDataEventoTrofeo(infoTrofeo.DataEventoStr);
                    infoTrofeo.TempoImNO = Convert.ToInt32(reader["TempoImNO"]);
                    infoTrofeo.TempoIppon = Convert.ToInt32(reader["TempoIppon"]);
                    infoTrofeo.TempoWari = Convert.ToInt32(reader["TempoWAri"]);
                    infoTrofeo.TempoYuko = Convert.ToInt32(reader["TempoYuko"]);

                    infoTrofeo.NumeroAtletiPartecipanti = 0;
                    infoTrofeo.NumeroCategoriePartecipanti = 0;
                    infoTrofeo.NumeroSocietaPartecipanti = 0;

                    using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                    {
                        TrofeoConfig dbTrofeoConfig = GetDBTrofeo(infoTrofeo, dbContext);
                        if(dbTrofeoConfig != null)
                        {
                            infoTrofeo.Id = dbTrofeoConfig.Id;
                            infoTrofeo.IdGuid = dbTrofeoConfig.IdGuid;
                        }
                    }

                    if (ReadSleepInterval >= 0) Thread.Sleep(ReadSleepInterval);
                }

                reader.Close();

                OnLogMsg("Recupero informazioni Trofeo terminato");
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadConfig error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void UpdateConfig()
        {
            infoTrofeo.NumeroAtletiPartecipanti = this.ElencoAtleti.Count;
            infoTrofeo.NumeroCategoriePartecipanti = this.ElencoCategorie.Count;
            infoTrofeo.NumeroSocietaPartecipanti = this.ElencoSocieta.Count;
        }

        private DateTime GetDataEventoTrofeo(string dataEventoStr)
        {
            DateTime dataEvento = DateTime.MinValue;

            if (string.IsNullOrWhiteSpace(dataEventoStr) == false)
            {
                DateTime outDateTime;

                if (DateTime.TryParse(dataEventoStr, out outDateTime))
                {
                    dataEvento = outDateTime;
                }
            }

            return dataEvento;
        }

        private void ReadAllData(OleDbConnection connection)
        {
            ReadCategories(connection);
        }

        #region Region Categorie 

        private void ReadCategories(OleDbConnection connection)
        {
            try
            {
                string queryString = "SELECT * FROM CategorieGC;";

                // Create the Command and Parameter objects.
                OleDbCommand command = new OleDbCommand(queryString, connection);
                //command.Parameters.AddWithValue("@pricePoint", paramValue);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.

                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    CategoriaInfo categoria = new CategoriaInfo();

                    categoria.Codice = Convert.ToString(reader["IDCatS"]).Trim();

                    var catInserita = FindCategoria(categoria.Codice);

                    if(catInserita == null)
                    {
                        categoria.Id = 0;

                        Guid? guid = GetDBRecordGuid<CategoriaInfo>(categoria);
                        categoria.IdGuid = guid.HasValue ? guid.Value : Guid.NewGuid();

                        categoria.TrofeoGuid = infoTrofeo.IdGuid;

                        categoria.Eta = Convert.ToString(reader["NomeCat"]).Trim();
                        categoria.Peso = Convert.ToString(reader["NomePeso"]).Trim();
                        categoria.Descrizione = string.Format("{0} - {1} Kg", categoria.Eta, categoria.Peso);
                        categoria.NumeroTatami = Convert.ToInt32(reader["Tavolo"]);
                        
                        categoria.Sesso = GetSesso(Convert.ToString(reader["Sesso"]).Trim());

                        var creata = Convert.ToBoolean(reader["Creata"]);
                        var iniziata = Convert.ToBoolean(reader["Iniziata"]);
                        var terminata = Convert.ToBoolean(reader["Terminata"]);

                        categoria.Stato = GetStatoCombattimento(creata, iniziata, terminata);

                        categoria.TipoGara = GetTipologiaGara(Convert.ToInt32(reader["Tipo"]) + 1); //in VB6 il tipo è definito da 0,1,2 -> mentre tipologiagara è definita 1,2,3

                        categoria.DurataExtraGara = Convert.ToInt32(reader["TempoG"]);
                        categoria.DurataGara = Convert.ToInt32(reader["TempoEx"]);
                    }
                    else
                    {
                        categoria = catInserita;

                        var creata = Convert.ToBoolean(reader["Creata"]);
                        var iniziata = Convert.ToBoolean(reader["Iniziata"]);
                        var terminata = Convert.ToBoolean(reader["Terminata"]);

                        categoria.NumeroTatami = Convert.ToInt32(reader["Tavolo"]);

                        categoria.Stato = GetStatoCombattimento(creata, iniziata, terminata);
                    }

                    ElencoCategorie[categoria.IdGuid] = categoria;

                    string msg = string.Format("Categoria: {0} - {1} è in stato= {2}", categoria.Descrizione, categoria.TipoGara, categoria.Stato);
                    OnLogMsg(msg);

                    // leggere atleti categoria
                    ReadAtlethsAndSocieties(connection, categoria);

                    msg = string.Format("Categoria: {0} conta {1} partecipanti", categoria.Descrizione, categoria.NumeroAtletiPartecipanti);
                    OnLogMsg(msg);

                    ReadReports(connection, categoria);

                    ReadClassificheAtletiCategoria(connection, categoria);

                    //ReadRankingSocieta(connection, categoria);

                    if (ReadSleepInterval >= 0) Thread.Sleep(ReadSleepInterval);
                }

                reader.Close();

            }
            catch(ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadCategories error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        #endregion

        #region Region Atleti / Societa

        private void ReadAtlethsAndSocieties(OleDbConnection connection, CategoriaInfo categoria)
        {
            try
            {
                string queryString = "SELECT * FROM PesiGC WHERE IDCatS = @cat;";

                // Create the Command and Parameter objects.
                OleDbCommand command = new OleDbCommand(queryString, connection);
                command.Parameters.AddWithValue("@cat", categoria.Codice);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.

                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    AtletaInfo atleta = new AtletaInfo();
                    atleta.Codice = Convert.ToString(reader["IDS"]).Trim();
                    atleta.Nome = Convert.ToString(reader["Nome"]).Trim();
                    atleta.Cognome = Convert.ToString(reader["Cognome"]).Trim();
                    atleta.NomeSocieta = Convert.ToString(reader["Societa"]).Trim();
                    atleta.Sesso = categoria.Sesso;
                    atleta.TrofeoGuid = infoTrofeo.IdGuid;
                    atleta.ID = 0;

                    var atletaInserito = FindAtleta(atleta.Nome, atleta.Cognome, atleta.NomeSocieta);

                    if(atletaInserito != null)
                    {
                        atleta.ID = atletaInserito.ID;
                        atleta.IdGuid = atletaInserito.IdGuid;

                        atleta.Anno = atletaInserito.Anno;
                        atleta.Sesso = atletaInserito.Sesso;
                        atleta.DescrizioneCategoria = atletaInserito.DescrizioneCategoria;
                        atleta.IDCategoria = atletaInserito.IDCategoria;

                        //codice è somma di tutti i codici atleta per accoppiata categoria/atleta
                        if (atletaInserito.Codice.Contains(atleta.Codice))
                        {
                            atleta.Codice = atletaInserito.Codice;
                        }
                        else
                        {
                            atleta.Codice = string.Format("{0};{1}", atleta.Codice, atletaInserito.Codice);
                        }

                    }
                    else
                    {
                        // nuovo atleta

                        Guid? guid = GetDBRecordGuid<AtletaInfo>(atleta);
                        atleta.IdGuid = guid.HasValue ? guid.Value : Guid.NewGuid();

                        atleta.Anno = categoria.Eta;
                        atleta.Sesso = categoria.Sesso;
                        atleta.DescrizioneCategoria = categoria.Descrizione;
                        atleta.IDCategoria = categoria.IdGuid;

                        // aggiungere atleta alla categoria
                        categoria.NumeroAtletiPartecipanti++;
                    }

                    var societa = FindSocieta(atleta.NomeSocieta);

                    if (societa == null)
                    {
                        //creare societa
                        societa = new SocietaInfo();

                        societa.Nome = atleta.NomeSocieta;
                        societa.Nazione = "";

                        Guid? sguid = GetDBRecordGuid<SocietaInfo>(societa);
                        societa.IdGuid = sguid.HasValue ? sguid.Value : Guid.NewGuid();

                        // aggiungere atleta alla societa
                        societa.NumeroAtleti++;

                        ElencoSocieta[societa.IdGuid] = societa;
                    }

                    societa.TrofeoGuid = infoTrofeo.IdGuid;
                    atleta.IDSocieta = societa.IdGuid;
                    
                    ElencoAtleti[atleta.IdGuid] = atleta;

                    string msg = string.Format("Atleta: {0} {1} Societa={2} Categoria={3}", atleta.Nome, atleta.Cognome, atleta.NomeSocieta, atleta.DescrizioneCategoria);
                    OnLogMsg(msg);

                    if (ReadSleepInterval >= 0) Thread.Sleep(ReadSleepInterval);
                }

                reader.Close();
  
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadAtlethsAndSocieties error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        #endregion

        #region Region Reports

        private void ReadReports(OleDbConnection connection, CategoriaInfo categoria)
        {
            try
            {
                StatoCategoria statoCategoria = FindStatoGestioneCategoria(categoria.IdGuid);

                if (statoCategoria == null)
                {
                    statoCategoria = new StatoCategoria();
                    statoCategoria.Codice = categoria.Codice;
                    statoCategoria.IdGuid = categoria.IdGuid;
                    statoCategoria.Stato = categoria.Stato;
                    statoCategoria.TipoGara = categoria.TipoGara;
                    statoCategoria.TrofeoGuid = infoTrofeo.IdGuid;

                    //statoCategoria.LivelloReportPrincipale = 64;
                    //statoCategoria.LivelloReportRecuperi = 64;
                }

                if(categoria.TipoGara == Enums.TipologiaGara.Italiana)
                {
                    ReadReportItaliana(connection, statoCategoria);
                }
                else
                {
                    ReadReportPrincipale(connection, statoCategoria);
                    ReadReportRecuperi(connection, statoCategoria);
                }

                StatoGestioneCategorie[categoria.IdGuid] = statoCategoria;
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReports error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        #region Region Reports Principale

        private void ReadReportPrincipale(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReportPrincipale64(connection, statoCategoria);
                ReadReportPrincipale32(connection, statoCategoria);
                ReadReportPrincipale16(connection, statoCategoria);
                ReadReportPrincipale8(connection, statoCategoria);
                ReadReportPrincipale4(connection, statoCategoria);
                ReadReportPrincipale2(connection, statoCategoria);
                ReadReportPrincipale1(connection, statoCategoria);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale64(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 64, "TPRD64", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale64 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale32(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {

                   ReadReport(connection, statoCategoria, 32, "TPRD32", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale32 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale16(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 16, "TPRD16", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale16 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale8(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 8, "TPRD8", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale8 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale4(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 4, "TPRD4", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale4 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale2(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 2, "TPRD2", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale2 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportPrincipale1(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 1, "TPRD1", Enums.TipologiaGara.DoppioTurnoPrincipale);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportPrincipale1 error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        #endregion

        #region Region Reports Recuperi

        private void ReadReportRecuperi(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReportRecuperi64(connection, statoCategoria);
                ReadReportRecuperi32(connection, statoCategoria);
                ReadReportRecuperi16(connection, statoCategoria);
                ReadReportRecuperi8(connection, statoCategoria);
                ReadReportRecuperi2B(connection, statoCategoria);
                ReadReportRecuperi2A(connection, statoCategoria);
                ReadReportRecuperi1(connection, statoCategoria);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi64(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 64, "TRRD64", Enums.TipologiaGara.DoppioTurnoRecupero);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi64 error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi32(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 32, "TRRD32", Enums.TipologiaGara.DoppioTurnoRecupero);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi32 error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi16(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 16, "TRRD16", Enums.TipologiaGara.DoppioTurnoRecupero);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi16 error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi8(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 8, "TRRD8", Enums.TipologiaGara.DoppioTurnoRecupero);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi8 error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi2B(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 4, "TRRD2B", Enums.TipologiaGara.DoppioTurnoRecupero);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi2B error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi2A(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 2, "TRRD2A", Enums.TipologiaGara.DoppioTurnoRecupero);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi2A error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadReportRecuperi1(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 1, "TRRD1", Enums.TipologiaGara.DoppioTurnoRecupero);

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportRecuperi1 error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        #endregion

        #region Region Reports Italiana

        private void ReadReportItaliana(OleDbConnection connection, StatoCategoria statoCategoria)
        {
            try
            {
                ReadReport(connection, statoCategoria, 1, "TGIT", Enums.TipologiaGara.Italiana);
                GeneraReportVincitoriGironeItaliana();
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReportItaliana error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void GeneraReportVincitoriGironeItaliana()
        {
            try
            {
                var reportVincitori = ElencoReports.Values.Where(r => r.InternalVincitore == true && r.TipologiaGara == Enums.TipologiaGara.Italiana).ToList();
                foreach (var rVinc in reportVincitori)
                {
                    ReportInfo report = new ReportInfo();

                    var nuovaPosizione = (int)Math.Ceiling(rVinc.InternalPosizione / 2.0);

                    var codiceUnivocoVincitore = GetCodicePosizione(nuovaPosizione, rVinc.TipologiaGara, 2); //secondo livello!!!

                    report.CodicePosizioneAtleta = codiceUnivocoVincitore;

                    var statoCategoriaCodice = rVinc.CodiceUnivoco.Split('_')[0];
                    var codiceAtleta = rVinc.CodiceUnivoco.Split('_')[2];

                    report.CodiceUnivoco = string.Format("{0}_{1}_{2}_{3}", statoCategoriaCodice, rVinc.TipologiaGara, codiceAtleta, report.CodicePosizioneAtleta);

                    var reportInserito = FindReport(report.CodiceUnivoco);

                    if (reportInserito == null)
                    {
                        report.IdGuid = Guid.NewGuid();
                        report.NumeroLivelliGara = 2;
                        report.TipologiaGara = rVinc.TipologiaGara;
                        report.Stato = rVinc.Stato;
                        report.IDCategoria = rVinc.IDCategoria;

                        Guid? guid = GetDBRecordGuid<ReportInfo>(report);
                        report.IdGuid = guid.HasValue ? guid.Value : Guid.NewGuid();
                    }
                    else
                    {
                        report.IdGuid = reportInserito.IdGuid;
                        report.NumeroLivelliGara = reportInserito.NumeroLivelliGara;
                        report.TipologiaGara = reportInserito.TipologiaGara;
                        report.Stato = reportInserito.Stato;
                        report.IDAtleta = reportInserito.IDAtleta;
                        report.NomeAtleta = reportInserito.NomeAtleta;
                        report.IDCategoria = reportInserito.IDCategoria;
                        report.IDSocieta = reportInserito.IDSocieta;
                    }

                    var atleta = FindAtleta(rVinc.IDAtleta);

                    if (atleta != null)
                    {
                        report.IDAtleta = atleta.IdGuid;

                        report.NomeAtleta = atleta.Nome;
                        report.CognomeAtleta = atleta.Cognome;
                        report.IDSocieta = atleta.IDSocieta;
                    }

                    report.TrofeoGuid = infoTrofeo.IdGuid;

                    ElencoReports[report.IdGuid] = report;

                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("GeneraReportVincitoriGironeItaliana error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        #endregion

        private bool ReadReport(OleDbConnection connection, StatoCategoria statoCategoria, int numeroLivelliGara, string tabella, Enums.TipologiaGara tipoGara)
        {
            try
            {
                string queryString = string.Format("SELECT * FROM {0} WHERE IDCatS = @cat AND Pres=1;", tabella);

                // Create the Command and Parameter objects.
                OleDbCommand command = new OleDbCommand(queryString, connection);
                command.Parameters.AddWithValue("@cat", statoCategoria.Codice);

                OleDbDataReader reader = command.ExecuteReader();

                bool foundData = false;

                int count = 0;

                while (reader.Read())
                {
                    foundData = true;

                    ReportInfo report = new ReportInfo();

                    var pos = Convert.ToInt32(reader["Pos"]);
                    var codiceAtleta = Convert.ToString(reader["IDAtleta"]);
                    var vincitore = Convert.ToBoolean(reader["Vitt"]);

                    report.CodicePosizioneAtleta = GetCodicePosizione(pos, tipoGara, numeroLivelliGara);

                    report.CodiceUnivoco = string.Format("{0}_{1}_{2}_{3}", statoCategoria.Codice, tipoGara, codiceAtleta, report.CodicePosizioneAtleta);

                    var reportInserito = FindReport(report.CodiceUnivoco);

                    if (reportInserito == null)
                    {
                        report.IdGuid = Guid.NewGuid();
                        report.NumeroLivelliGara = numeroLivelliGara;
                        report.TipologiaGara = tipoGara;
                        report.Stato = statoCategoria.Stato;
                        report.IDCategoria = statoCategoria.IdGuid;

                        Guid? guid = GetDBRecordGuid<ReportInfo>(report);
                        report.IdGuid = guid.HasValue ? guid.Value : Guid.NewGuid();
                    }
                    else
                    {
                        report.IdGuid = reportInserito.IdGuid;
                        report.NumeroLivelliGara = numeroLivelliGara;
                        report.TipologiaGara = reportInserito.TipologiaGara;
                        report.Stato = reportInserito.Stato;
                        report.IDAtleta = reportInserito.IDAtleta;
                        report.NomeAtleta = reportInserito.NomeAtleta; 
                        report.IDCategoria = reportInserito.IDCategoria;
                        report.IDSocieta = reportInserito.IDSocieta;
                        
                    }

                    //for internal use only
                    //serve per gestire il vincitore nel girone all'italiana
                    report.InternalVincitore = vincitore;
                    report.InternalPosizione = pos;

                    var atleta = FindAtleta(codiceAtleta);

                    if (atleta != null)
                    {
                        report.IDAtleta = atleta.IdGuid;

                        report.NomeAtleta = atleta.Nome;
                        report.CognomeAtleta = atleta.Cognome;
                        report.IDSocieta = atleta.IDSocieta;
                    }

                    report.TrofeoGuid = infoTrofeo.IdGuid;

                    ElencoReports[report.IdGuid] = report;

                    count++;

                    string msg = string.Format("Categoria: {4} Tipologia: {0} Livello: {1} Atleta: {2} Posizione: {3}", report.TipologiaGara, report.NumeroLivelliGara, report.NomeAtleta, report.CodicePosizioneAtleta, statoCategoria.Codice);
                    OnLogMsg(msg);

                    if (ReadSleepInterval >= 0) Thread.Sleep(ReadSleepInterval);
                }
   
                reader.Close();

                return foundData;
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadReport error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }

            return false;
        }

        private string GetCodicePosizione(int posizione, Enums.TipologiaGara tipoGara, int numeroLivelliGara)
        {
            try 
            {
                int livello = 0;

                if(tipoGara == Enums.TipologiaGara.DoppioTurnoPrincipale)
                {
                    if(numeroLivelliGara == 64)
                    {
                        livello = 1;
                    }
                    if (numeroLivelliGara == 32)
                    {
                        livello = 2;  
                    }
                    if (numeroLivelliGara == 16)
                    {
                        livello = 3;
                    }
                    if (numeroLivelliGara == 8)
                    {
                        livello = 4;
                    }
                    if (numeroLivelliGara == 4)
                    {
                        livello = 5;
                    }
                    if (numeroLivelliGara == 2)
                    {
                        livello = 6;
                    }
                    if (numeroLivelliGara == 1)
                    {
                        livello = 7;
                    }
                }

                if (tipoGara == Enums.TipologiaGara.DoppioTurnoRecupero)
                {
                    if (numeroLivelliGara == 64)
                    {
                        livello = 1;
                    }
                    if (numeroLivelliGara == 32)
                    {
                        livello = 2;
                    }
                    if (numeroLivelliGara == 16)
                    {
                        livello = 3;
                    }
                    if (numeroLivelliGara == 8)
                    {
                        livello = 4;
                    }
                    if (numeroLivelliGara == 4)
                    {
                        livello = 5;
                    }
                    if (numeroLivelliGara == 2)
                    {
                        livello = 6;
                    }
                    if (numeroLivelliGara == 1)
                    {
                        livello = 7;
                    }
                }

                if (tipoGara == Enums.TipologiaGara.Italiana)
                {
                    livello = numeroLivelliGara;
                }

                var codice = string.Format("r{0}_{1}", livello, posizione);
                return codice;
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("GetCodicePosizione error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }

            return string.Empty;
        }

        #endregion

        #region Region Classifiche 

        private void ReadClassificheAtletiCategoria(OleDbConnection connection, CategoriaInfo categoria)
        {
            try
            {
                string queryString = "SELECT * FROM PesiGC WHERE IDCatS = @cat;";

                // Create the Command and Parameter objects.
                OleDbCommand command = new OleDbCommand(queryString, connection);
                command.Parameters.AddWithValue("@cat", categoria.Codice);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.

                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    AtletaInfo atleta = new AtletaInfo();
                    atleta.Nome = Convert.ToString(reader["Nome"]).Trim();
                    atleta.Cognome = Convert.ToString(reader["Cognome"]).Trim();
                    atleta.NomeSocieta = Convert.ToString(reader["Societa"]).Trim();

                    int posizioneClassificaAtleta = Convert.ToInt32(reader["Classifica"]);

                    var atletaInserito = FindAtleta(atleta.Nome, atleta.Cognome, atleta.NomeSocieta);

                    if (atletaInserito != null)
                    {
                        ClassificaAtletaCategoriaInfo classifica = new ClassificaAtletaCategoriaInfo();

                        //setto terna chiavi classifica categoria atleta
                        classifica.TrofeoGuid = infoTrofeo.IdGuid;
                        classifica.CategoriaGuid = categoria.IdGuid;
                        classifica.AtletaGuid = atletaInserito.IdGuid;

                        var classificaInserita = FindClassificaCategoriaAtleta(classifica.TrofeoGuid, classifica.CategoriaGuid, classifica.AtletaGuid);

                        if (classificaInserita != null)
                        {
                            classifica.IdGuid = classificaInserita.IdGuid;
                        }
                        else
                        {
                            Guid? guid = GetDBRecordGuid<ClassificaAtletaCategoriaInfo>(classifica);
                            classifica.IdGuid = guid.HasValue ? guid.Value : Guid.NewGuid();
                        }                        

                        //setto posizione classifica
                        classifica.PosizioneClassifica = posizioneClassificaAtleta;

                        ElencoClassificheCategorie[classifica.IdGuid] = classifica;
                    }

                }
                reader.Close();

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("ReadClassificheAtletiCategoria error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void ReadRankingSocieta(OleDbConnection connection, CategoriaInfo categoria)
        {
            try
            {
                string queryString = "SELECT * FROM xxx WHERE IDCatS = @cat;";

                // Create the Command and Parameter objects.
                OleDbCommand command = new OleDbCommand(queryString, connection);
                command.Parameters.AddWithValue("@cat", categoria.Codice);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.

                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                }

                reader.Close();

            }
            catch (ThreadAbortException)
            {
                ;
            }
            catch (Exception ex)
            {
                string error = string.Format("ReadRankingSocieta error: {0}", Utils.GetFullExceptionTrace(ex));
                System.Diagnostics.Trace.TraceError(error);
                OnLogError(error);
            }
        }

        #endregion

        #endregion

        #region Region SCRITTURA DATI 

        private readonly int WriteSleepInterval = Properties.Settings.Default.WriteSleepInterval;

        private void WriteAllData()
        {
            try
            {
                string msg = "Processo scrittura iniziato";
                OnLogMsg(msg);

                WriteTrofeoConfig();
                WriteElencoSocieta();
                WriteElencoCategorie();
                WriteElencoAtleti();
                WriteElencoReports();
                WriteClassificheAtletiCategoria();

                TraceWriteData();

                msg = "Processo scrittura completato correttamente";
                OnLogMsg(msg);
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteAllData error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void WriteTrofeoConfig()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    TrofeoConfig dbTrofeoConfig = GetDBTrofeo(infoTrofeo, dbContext);

                    if (dbTrofeoConfig == null)
                    {
                        dbTrofeoConfig = dbContext.TrofeoConfigSet.Create();

                        dbContext.TrofeoConfigSet.Add(dbTrofeoConfig);

                        dbTrofeoConfig.IdGuid = infoTrofeo.IdGuid;
                    }

                    infoTrofeo.IdGuid = dbTrofeoConfig.IdGuid;
                    dbTrofeoConfig.Titolo = infoTrofeo.Titolo;

                    dbTrofeoConfig.DataEvento = infoTrofeo.DataEvento;
                    dbTrofeoConfig.DataEventoStr = infoTrofeo.DataEventoStr;
                    dbTrofeoConfig.TempoImNO = infoTrofeo.TempoImNO;
                    dbTrofeoConfig.TempoIppon = infoTrofeo.TempoIppon;
                    dbTrofeoConfig.TempoWari = infoTrofeo.TempoWari;
                    dbTrofeoConfig.TempoYuko = infoTrofeo.TempoYuko;

                    dbTrofeoConfig.NumeroAtletiPartecipanti = infoTrofeo.NumeroAtletiPartecipanti;
                    dbTrofeoConfig.NumeroCategoriePartecipanti = infoTrofeo.NumeroCategoriePartecipanti;
                    dbTrofeoConfig.NumeroSocietaPartecipanti = infoTrofeo.NumeroSocietaPartecipanti;

                    CheckSaveDbContext(dbContext, true);

                    string msg = string.Format("Config trofeo [{0}] salvate correttamente", infoTrofeo.Titolo);
                    OnLogMsg(msg);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteTrofeoConfig error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void WriteElencoSocieta()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    var dbTrofeo = GetDBTrofeo(InfoTrofeo, dbContext);

                    if (dbTrofeo == null)
                        throw new Exception("ERRORE: nessun trofeo initializzato in DB!");

                    var elencoSocieta = ElencoSocieta.Values.ToList();

                    foreach (var societa in elencoSocieta)
                    {
                        Societa dbSocieta = dbContext.SocietaSet.FirstOrDefault(s => s.IdGuid == societa.IdGuid);

                        if(dbSocieta == null)
                        {
                            dbSocieta = dbContext.SocietaSet.Create();

                            dbSocieta.Trofeo = dbTrofeo;

                            dbContext.SocietaSet.Add(dbSocieta);
                        }

                        dbSocieta.IdGuid = societa.IdGuid;
                        dbSocieta.Nazione = societa.Nazione;
                        dbSocieta.Nome = societa.Nome;
                        dbSocieta.PunteggioTotale = societa.PunteggioTotale;
                        dbSocieta.Classifica = societa.Classifica;

                        dbSocieta.TrofeoGuid = InfoTrofeo.IdGuid;

                        CheckSaveDbContext(dbContext);
                    }

                    CheckSaveDbContext(dbContext, true);

                    string msg = string.Format("Salvate correttamente {0} societa", elencoSocieta.Count);
                    OnLogMsg(msg);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteElencoSocieta error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void WriteElencoCategorie()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    var dbTrofeo = GetDBTrofeo(InfoTrofeo, dbContext);

                    if (dbTrofeo == null)
                        throw new Exception("ERRORE: nessun trofeo initializzato in DB!");

                    var elencoCategorie = ElencoCategorie.Values.ToList();

                    foreach (var categoria in elencoCategorie)
                    {
                        Categoria dbCategoria = dbContext.CategoriaSet.FirstOrDefault(c => c.IdGuid == categoria.IdGuid);

                        if (dbCategoria == null)
                        {
                            dbCategoria = dbContext.CategoriaSet.Create();
                            dbCategoria.Trofeo = dbTrofeo;
                            dbContext.CategoriaSet.Add(dbCategoria);
                        }

                        dbCategoria.IdGuid = categoria.IdGuid;
                        dbCategoria.Codice = categoria.Codice;
                        dbCategoria.Descrizione = categoria.Descrizione;
                        dbCategoria.DurataExtraGara = categoria.DurataGara;
                        dbCategoria.DurataGara = categoria.DurataExtraGara;
                        dbCategoria.Eta = categoria.Eta;
                        dbCategoria.NumeroTatami = categoria.NumeroTatami;
                        dbCategoria.Peso = categoria.Peso;
                        dbCategoria.Sesso = ((int)categoria.Sesso).ToString();
                        dbCategoria.Stato = (StatoCombattimento)((int)categoria.Stato);
                        dbCategoria.TipologiaGara = (TipologiaGara)((int)categoria.TipoGara);

                        dbCategoria.TrofeoGuid = InfoTrofeo.IdGuid;

                        CheckSaveDbContext(dbContext);
                    }

                    CheckSaveDbContext(dbContext, true);

                    string msg = string.Format("Salvate correttamente {0} categorie", elencoCategorie.Count);
                    OnLogMsg(msg);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteElencoCategorie error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void WriteElencoAtleti()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    var elencoAtleti = ElencoAtleti.Values.ToList();

                    foreach (var atleta in elencoAtleti)
                    {
                        var dbCategoria = dbContext.CategoriaSet.FirstOrDefault(c => c.IdGuid == atleta.IDCategoria);

                        if (dbCategoria == null)
                            continue;

                        var dbSocieta = dbContext.SocietaSet.FirstOrDefault(s => s.IdGuid == atleta.IDSocieta);

                        if (dbSocieta == null)
                            continue;

                        Atleta dbAtleta = dbContext.AtletaSet.FirstOrDefault(a => a.IdGuid == atleta.IdGuid);

                        if (dbAtleta == null)
                        {
                            dbAtleta = dbContext.AtletaSet.Create();

                            dbAtleta.Societa = dbSocieta;

                            dbContext.AtletaSet.Add(dbAtleta);
                        }

                        if(dbCategoria.ElencoAtleti.FirstOrDefault(a => a.IdGuid == dbAtleta.IdGuid) == null)
                        {
                            dbCategoria.ElencoAtleti.Add(dbAtleta);
                        }

                        dbAtleta.IdGuid = atleta.IdGuid;
                        dbAtleta.Anno = atleta.Anno;
                        dbAtleta.Cognome = atleta.Cognome;
                        dbAtleta.Nome = atleta.Nome;
                        dbAtleta.Sesso = (TipologiaSesso)((int)(atleta.Sesso));
                        dbAtleta.Codice = atleta.Codice;
                        dbAtleta.Ranking = atleta.Ranking;

                        dbAtleta.TrofeoGuid = InfoTrofeo.IdGuid;

                        CheckSaveDbContext(dbContext);
                    }

                    CheckSaveDbContext(dbContext, true);

                    string msg = string.Format("Salvati correttamente {0} atleti", elencoAtleti.Count);
                    OnLogMsg(msg);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteElencoAtleti error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void WriteElencoReports()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    var elencoReports = ElencoReports.Values.ToList();

                    foreach (var report in elencoReports)
                    {
                        var dbCategoria = dbContext.CategoriaSet.FirstOrDefault(c => c.IdGuid == report.IDCategoria);

                        if (dbCategoria == null)
                            continue;

                        var dbAtleta = dbContext.AtletaSet.FirstOrDefault(s => s.IdGuid == report.IDAtleta);

                        if (dbAtleta == null)
                            continue;

                        Report dbReport = dbContext.ReportSet.FirstOrDefault(r => r.IdGuid == report.IdGuid);

                        if (dbReport == null)
                        {
                            dbReport = dbContext.ReportSet.Create();

                            dbCategoria.ElencoReports.Add(dbReport);
                            dbReport.Categoria = dbCategoria;
                            dbAtleta.ElencoReports.Add(dbReport);

                            dbContext.ReportSet.Add(dbReport);
                        }

                        dbReport.IdGuid = report.IdGuid;
                        dbReport.CodicePosizioneAtleta = report.CodicePosizioneAtleta;
                        dbReport.NumeroLivelliGara = report.NumeroLivelliGara;
                        dbReport.Stato = (StatoCombattimento)((int)report.Stato);
                        dbReport.TipologiaGara = (TipologiaGara)((int)report.TipologiaGara);
                        dbReport.CodiceUnivoco = report.CodiceUnivoco;

                        dbReport.TrofeoGuid = InfoTrofeo.IdGuid;

                        if (dbReport.Atleta.IdGuid != report.IDAtleta)
                        {
                            dbReport.Atleta.ElencoReports.Remove(dbReport);
                            dbAtleta.ElencoReports.Add(dbReport);
                            dbReport.Atleta = dbAtleta;
                            
                        }

                        CheckSaveDbContext(dbContext);
                    }

                    CheckSaveDbContext(dbContext, true);

                    string msg = string.Format("Salvati correttamente {0} reports", elencoReports.Count);
                    OnLogMsg(msg);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteElencoReports error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        private void WriteClassificheAtletiCategoria()
        {
            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    var dbTrofeo = GetDBTrofeo(InfoTrofeo, dbContext);

                    if (dbTrofeo == null)
                        throw new Exception("ERRORE: nessun trofeo initializzato in DB!");

                    var elencoClassifiche = ElencoClassificheCategorie.Values.ToList();

                    foreach (var classifica in elencoClassifiche)
                    {
                        var dbCategoria = dbContext.CategoriaSet.FirstOrDefault(c => c.IdGuid == classifica.CategoriaGuid);

                        if (dbCategoria == null)
                            continue;

                        Atleta dbAtleta = dbContext.AtletaSet.FirstOrDefault(a => a.IdGuid == classifica.AtletaGuid);

                        if (dbAtleta == null)
                            continue;

                        ClassificaCategoria dbClassifica = dbContext.ClassificaCategoriaSet.FirstOrDefault(cl => cl.IdGuid == classifica.IdGuid);

                        if (dbClassifica == null)
                        {
                            dbClassifica = dbContext.ClassificaCategoriaSet.Create();

                            dbClassifica.Trofeo = dbTrofeo;

                            dbClassifica = dbContext.ClassificaCategoriaSet.Add(dbClassifica);
                        }

                        dbClassifica.IdGuid = classifica.IdGuid;
                        dbClassifica.CategoriaId = dbCategoria.IdGuid;
                        dbClassifica.AtletaId = dbAtleta.IdGuid;
                        dbClassifica.Posizione = classifica.PosizioneClassifica;

                        dbClassifica.TrofeoGuid = InfoTrofeo.IdGuid;

                        CheckSaveDbContext(dbContext);
                    }

                    CheckSaveDbContext(dbContext, true);

                    string msg = string.Format("Salvati correttamente {0} posizioni classifiche categoria - atleta", elencoClassifiche.Count);
                    OnLogMsg(msg);
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteClassificheAtletiCategoria error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
        }

        #endregion

        #region Region Metodi Accessori DATI 

        private TrofeoConfig GetDBTrofeo(TrofeoInfo trofeo, JudoModelContainer dbContext)
        {
            TrofeoConfig dbTrofeoConfig = dbContext.TrofeoConfigSet.FirstOrDefault(t => t.Titolo == infoTrofeo.Titolo);
            return dbTrofeoConfig;
        }

        private Guid? GetDBRecordGuid<T>(T t) where T : class
        {
            Guid? recordGuid = null;

            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    Type tType = t.GetType();

                    if(tType == typeof(TrofeoInfo))
                    {
                        TrofeoInfo tinfo = t as TrofeoInfo;
                        recordGuid = dbContext.TrofeoConfigSet.FirstOrDefault(e => e.Titolo == tinfo.Titolo)?.IdGuid;
                    }
                    if (tType == typeof(AtletaInfo))
                    {
                        AtletaInfo a = t as AtletaInfo;
                        recordGuid = dbContext.AtletaSet.FirstOrDefault(e => string.Compare(e.Nome, a.Nome, true) == 0 && string.Compare(e.Cognome, a.Cognome, true) == 0 && (e.Societa != null ? string.Compare(e.Societa.Nome, a.NomeSocieta, true) == 0 : true) && e.TrofeoGuid == InfoTrofeo.IdGuid)?.IdGuid;
                    }
                    if (tType == typeof(CategoriaInfo))
                    {
                        CategoriaInfo c = t as CategoriaInfo;
                        recordGuid = dbContext.CategoriaSet.FirstOrDefault(e => e.Codice == c.Codice && e.TrofeoGuid == InfoTrofeo.IdGuid)?.IdGuid;
                    }
                    if (tType == typeof(ClassificaAtletaCategoriaInfo))
                    {
                        ClassificaAtletaCategoriaInfo cc = t as ClassificaAtletaCategoriaInfo;
                        recordGuid = dbContext.ClassificaCategoriaSet.FirstOrDefault(e => e.AtletaId == cc.AtletaGuid && e.CategoriaId == cc.CategoriaGuid  && e.TrofeoGuid == InfoTrofeo.IdGuid)?.IdGuid;
                    }
                    if (tType == typeof(ReportInfo))
                    {
                        ReportInfo r = t as ReportInfo;
                        recordGuid = dbContext.ReportSet.FirstOrDefault(e=> e.CodiceUnivoco == r.CodiceUnivoco && e.TrofeoGuid == InfoTrofeo.IdGuid)?.IdGuid;
                    }
                    if (tType == typeof(SocietaInfo))
                    {
                        SocietaInfo s = t as SocietaInfo;
                        recordGuid = dbContext.SocietaSet.FirstOrDefault(e => string.Compare(e.Nome.Trim(), s.Nome.Trim()) == 0 && e.TrofeoGuid == InfoTrofeo.IdGuid)?.IdGuid;
                    }
                }
            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("WriteTrofeoConfig error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }
            return recordGuid;
        }

        int counter = 0;

        private void CheckSaveDbContext(JudoModelContainer dbContext, bool forceSave = false)
        {
            counter++;

            if (forceSave || counter >= 100)
            {
                OnLogMsg("saving data..");
                counter = 0;
                dbContext.SaveChanges();

                if(WriteSleepInterval >= 0) Thread.Sleep(WriteSleepInterval);
            }
        }

        private void TraceReadData()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("***************************************");
            sb.AppendLine("READ STATE:");
            sb.AppendLine(string.Format("Numero Atleti Partecipanti: {0}", infoTrofeo.NumeroAtletiPartecipanti));
            sb.AppendLine(string.Format("Numero Categorie Partecipanti: {0}", infoTrofeo.NumeroCategoriePartecipanti));
            sb.AppendLine(string.Format("Numero Societa Partecipanti: {0}", infoTrofeo.NumeroSocietaPartecipanti));
            sb.AppendLine(string.Format("Numero Reports Letti: {0}", this.ElencoReports.Count));
            sb.AppendLine(string.Format("Numero Classifiche Lette: {0}", this.ElencoClassificheCategorie.Count));
            sb.AppendLine("***************************************");

            OnLogMsg(sb.ToString());
        }

        private void TraceWriteData()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("***************************************");
            sb.AppendLine("WRITTEN STATE:");

            try
            {
                using (var dbContext = DataAccessContext.JudoModelContainerDbContext)
                {
                    sb.AppendLine(string.Format("Numero Atleti Partecipanti: {0}", dbContext.AtletaSet.Where(e => e.TrofeoGuid == InfoTrofeo.IdGuid).Count()));
                    sb.AppendLine(string.Format("Numero Categorie Partecipanti: {0}", dbContext.CategoriaSet.Where(e => e.TrofeoGuid == InfoTrofeo.IdGuid).Count()));
                    sb.AppendLine(string.Format("Numero Societa Partecipanti: {0}", dbContext.SocietaSet.Where(e => e.TrofeoGuid == InfoTrofeo.IdGuid).Count()));
                    sb.AppendLine(string.Format("Numero Reports Letti: {0}", dbContext.ReportSet.Where(e => e.TrofeoGuid == InfoTrofeo.IdGuid).Count()));
                    sb.AppendLine(string.Format("Numero Classifiche Lette: {0}", dbContext.ClassificaCategoriaSet.Where(e => e.TrofeoGuid == InfoTrofeo.IdGuid).Count()));
                }

            }
            catch (ThreadAbortException) { }
            catch (Exception ex)
            {
                string error = string.Format("TraceWriteData error: {0}", Utils.GetFullExceptionTrace(ex));
                OnLogError(error);
            }

            sb.AppendLine("***************************************");

            OnLogMsg(sb.ToString());
        }

        private ReportInfo FindReport(string codiceUnivoco)
        {
            var reportInserito = ElencoReports.Values.FirstOrDefault(r => r.CodiceUnivoco == codiceUnivoco);
            return reportInserito;
        }

        private AtletaInfo FindAtleta(string nome, string cognome, string societa)
        {
            var atleta = ElencoAtleti.Values.FirstOrDefault(a => a.Nome == nome && a.Cognome == cognome && a.NomeSocieta == societa);
            return atleta;
        }

        private AtletaInfo FindAtleta(string codiceAtleta)
        {
            var atleta = ElencoAtleti.Values.FirstOrDefault(a => a.Codice.Contains(codiceAtleta));
            return atleta;
        }

        private AtletaInfo FindAtleta(Guid idGuidAtleta)
        {
            var atleta = ElencoAtleti.Values.FirstOrDefault(a => a.IdGuid == idGuidAtleta);
            return atleta;
        }

        private CategoriaInfo FindCategoria(string idcategoria)
        {
            var catInserita = ElencoCategorie.Values.FirstOrDefault(c => c.Codice == idcategoria);
            return catInserita;
        }

        private ClassificaAtletaCategoriaInfo FindClassificaCategoriaAtleta(Guid idGuidTrofeo, Guid idGuidCategoria, Guid idGuidAtleta)
        {
            return ElencoClassificheCategorie.Values.FirstOrDefault(cl => cl.TrofeoGuid == idGuidTrofeo && cl.CategoriaGuid == idGuidCategoria && cl.AtletaGuid == idGuidAtleta);
        }

        private StatoCategoria FindStatoGestioneCategoria(Guid idcategoria)
        {
            StatoCategoria statoCategoria = null;

            if (StatoGestioneCategorie.ContainsKey(idcategoria))
            {
                statoCategoria = StatoGestioneCategorie[idcategoria];
            }

            return statoCategoria;
        }

        private SocietaInfo FindSocieta(string nomeSocieta)
        {
            var socInserita = ElencoSocieta.Values.FirstOrDefault(s => s.Nome == nomeSocieta);
            return socInserita;
        }

        private Enums.TipologiaGara GetTipologiaGara(int tipo)
        {
            Enums.TipologiaGara tipoGara = Enums.TipologiaGara.DoppioTurnoRecupero;

            if (Enum.IsDefined(typeof(Enums.TipologiaGara), tipo))
            {
                tipoGara = (Enums.TipologiaGara)tipo;
            }

            return tipoGara;
        }

        private Enums.StatoCombattimento GetStatoCombattimento(bool creata, bool iniziata, bool terminata)
        {
            Enums.StatoCombattimento stato = Enums.StatoCombattimento.InAttesa;

            if (iniziata)
            {
                stato = Enums.StatoCombattimento.InCorso;
            }

            if (terminata)
            {
                stato = Enums.StatoCombattimento.Concluso;
            }

            return stato;
        }

        private Enums.TipoSesso GetSesso(string sessoStr)
        {
            Enums.TipoSesso sesso = Enums.TipoSesso.NonSpecificato;

            if (string.Compare(sessoStr, "M", true) == 0)
            {
                sesso = Enums.TipoSesso.Maschio;
            }
            else if (string.Compare(sessoStr, "F", true) == 0)
            {
                sesso = Enums.TipoSesso.Femmina;
            }

            return sesso;
        }

        #endregion

        #endregion
    }
}
