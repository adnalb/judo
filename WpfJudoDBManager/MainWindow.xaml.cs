﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JudoVisualBasicToNetDBReader;

namespace WpfJudoDBManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string VbDbPath = string.Empty;
        private VbToNetManager vbToNetManager = null;

        public MainWindow()
        {
            InitializeComponent();

            this.ButtonStart.IsEnabled = true;
            this.ButtonStop.IsEnabled = false;
            this.ListBoxMsgs.Items.Clear();

            if(string.IsNullOrEmpty(Properties.Settings.Default.VbPath))
            {
                VbDbPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Data", "Tabelloni.mdb");
            }
            else
            {
                VbDbPath = Properties.Settings.Default.VbPath;
            }
            vbToNetManager = new VbToNetManager(VbDbPath);
            vbToNetManager.LogMsg += vbToNetManager_LogMsg;
            vbToNetManager.LogError += vbToNetManager_LogError;

            vbDBPathLabel.Content = VbDbPath;

            this.ListBoxMsgs.MouseRightButtonUp += ListBoxMsgs_MouseRightButtonUp;
        }

        void vbToNetManager_LogError(object sender, string e)
        {
            LogMsg(e, true);
        }

        void vbToNetManager_LogMsg(object sender, string e)
        {
            LogMsg(e);
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ButtonStart.IsEnabled = false;

                this.ListBoxMsgs.Items.Clear();

                LogMsg("Start Reading Data..", false, false);

                this.Cursor = Cursors.Wait;

                if (System.Threading.ThreadPool.QueueUserWorkItem(ExecuteStart) == false)
                {
                    ExecuteStart(null);
                }
            }
            catch (Exception ex)
            {
                LogMsg(string.Format("Start Error: {0}", ex), true);
                MessageBox.Show(string.Format("Error: {0}", ex), "Start", MessageBoxButton.OK, MessageBoxImage.Error);

                this.ButtonStart.IsEnabled = true;
            }
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.ButtonStop.IsEnabled = false;

                LogMsg("Stop Reading Data..", false, false);

                this.Cursor = Cursors.Wait;

                if (System.Threading.ThreadPool.QueueUserWorkItem(ExecuteStop) == false)
                {
                    ExecuteStop(null);
                }
            }
            catch (Exception ex)
            {
                LogMsg(string.Format("Stop Error: {0}", ex), true);
                MessageBox.Show(string.Format("Error: {0}", ex), "Stop", MessageBoxButton.OK, MessageBoxImage.Error);

                this.ButtonStop.IsEnabled = true;
            }
        }

        private void ListBoxMsgs_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            foreach (Label label in this.ListBoxMsgs.SelectedItems)
            {
                sb.AppendLine(label.Content.ToString());
            }

            MessageBox.Show(sb.ToString());

        }

        void ListBoxMsgs_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.ListBoxMsgs.Items.Clear();
        }

        private void ExecuteStart(object obj)
        {
            System.Threading.Thread.Sleep(100);

            this.Dispatcher.Invoke(new System.Action(() =>
            {
                try
                {
                    vbToNetManager.StartVbDataReading();

                    this.ButtonStop.IsEnabled = true;

                    LogMsg("Start OK");
                }
                catch (Exception ex)
                {
                    LogMsg(string.Format("Start Error: {0}", ex), true);
                    MessageBox.Show(string.Format("Error: {0}", ex), "Start", MessageBoxButton.OK, MessageBoxImage.Error);

                    this.ButtonStart.IsEnabled = true;
                }

            }));

            ResetCursor();
        }

        private void ExecuteStop(object obj)
        {
            System.Threading.Thread.Sleep(100);

            this.Dispatcher.Invoke(new System.Action(() =>
            {
                try
                {
                    vbToNetManager.StopVbDataReading();

                    this.ButtonStart.IsEnabled = true;

                    LogMsg("Stop OK");
                }
                catch (Exception ex)
                {
                    LogMsg(string.Format("Stop Error: {0}", ex), true);
                    MessageBox.Show(string.Format("Error: {0}", ex), "Stop", MessageBoxButton.OK, MessageBoxImage.Error);

                    this.ButtonStop.IsEnabled = true;
                }

            }));

            ResetCursor();
        }

        private void ResetCursor()
        {
            this.Dispatcher.Invoke(new System.Action(() =>
            {
                this.Cursor = Cursors.Arrow;
            }));
        }

        private void LogMsg(string msg, bool isError = false, bool withDispatcher = true)
        {
            try
            {
                if (withDispatcher)
                {
                    this.Dispatcher.BeginInvoke(new System.Action(() =>
                    {
                        WriteLogMsg(msg, isError);
                    }));
                }
                else
                {
                    WriteLogMsg(msg, isError);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error: {0}", ex), "LogMsg", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void WriteLogMsg(string msg, bool isError)
        {
            Label label = new Label();
            label.Content = msg;
            if (isError)
            {
                label.Background = Brushes.Red;
            }
            this.ListBoxMsgs.Items.Insert(0, label);

            if (this.ListBoxMsgs.Items.Count > 1000)
            {
                this.ListBoxMsgs.Items.RemoveAt((this.ListBoxMsgs.Items.Count - 1));
            }

            System.Diagnostics.Debug.WriteLine(string.Format("[ERROR={0}] {1}", isError, msg));

            if(isError)
            {
                System.Diagnostics.Trace.TraceError(msg);
            }

        }


    }
}
