﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class AtletaInfo
    {
        public Guid IdGuid { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Anno { get; set; }
        public string Eta { get; set; }
        public string Peso { get; set; }

        public Enums.TipoSesso Sesso { get; set; }

        public string NomeSocieta { get; set; }
        public string DescrizioneCategoria { get; set; }

        public string Codice { get; set; }
        public int ID { get; set; }
        public Guid IDSocieta { get; set; }
        public Guid IDCategoria { get; set; }
        public int PosizioneClassifica { get; set; }

        public int Ranking { get; set; } //valore testa di serie

        public Guid TrofeoGuid { get; set; }

        public List<CategoriaInfo> ElencoCategorie { get; set; }
    }
}
