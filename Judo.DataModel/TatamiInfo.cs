﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class TatamiInfo
    {
        public int NumeroTatami { get; set; }
        public CategoriaInfo CategoriaInCorso { get; set; }
        public List<CategoriaInfo> ElencoCategorie { get; set; }

        public Guid TrofeoGuid { get; set; }
    }
}
