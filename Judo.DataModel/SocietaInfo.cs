﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class SocietaInfo
    {
        public Guid IdGuid { get; set; }
        public string Nome { get; set; }
        public string Nazione { get; set; }
        public int PunteggioTotale { get; set; }
        public int Classifica{ get; set; }
        public int NumeroAtleti { get; set; }

        public Guid TrofeoGuid { get; set; }

        public int ID { get; set; }

    }
}
