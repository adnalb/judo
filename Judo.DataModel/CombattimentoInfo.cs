﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class CombattimentoInfo
    {
        public Guid IdGuid { get; set; }

        public string Categoria { get; set; }
        public string NumeroTatami { get; set; }
        
        public string Vincente { get; set; }
        public Enums.StatoCombattimento Stato { get; set; }
        public string IdReportAtletaA { get; set; }
        public string IdReportAtletaB { get; set; }
        public string AtletaA { get; set; }
        public string AtletaB { get; set; }
        public string CodicePosizioneAtletaA { get; set; }
        public string CodicePosizioneAtletaB { get; set; }
        public string DataCombattimento { get; set; }

        public Guid TrofeoGuid { get; set; }

        public int Id { get; set; }
    }
}
