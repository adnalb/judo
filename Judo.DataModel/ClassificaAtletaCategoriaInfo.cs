﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class ClassificaAtletaCategoriaInfo
    {
        public int Id { get; set; }

        public Guid IdGuid { get; set; }
        public Guid TrofeoGuid { get; set; }
        public Guid CategoriaGuid { get; set; }
        public Guid AtletaGuid { get; set; }
        public int PosizioneClassifica { get; set; }
    }
}
