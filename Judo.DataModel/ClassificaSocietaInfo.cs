﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class ClassificaSocietaInfo
    {
        public Guid TrofeoGuid { get; set; }

        List<SocietaInfo> ElencoClassificaSocieta { get; set; }
    }
}
