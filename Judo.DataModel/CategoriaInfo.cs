﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class CategoriaInfo
    {

        public Guid IdGuid { get; set; }
        public string Codice { get; set; }
        public string Descrizione { get; set; }
        public Enums.TipoSesso Sesso { get; set; }
        public string Peso { get; set; }
        public string Eta { get; set; }

        public int NumeroTatami { get; set; }
        public int NumeroAtletiPartecipanti { get; set; }
        public int DurataGara { get; set; }
        public int DurataExtraGara { get; set; }
        public Enums.StatoCombattimento Stato { get; set; }
        public Enums.TipologiaGara TipoGara { get; set; }

        public Guid TrofeoGuid { get; set; }

        public int Id { get; set; }
    }
}
