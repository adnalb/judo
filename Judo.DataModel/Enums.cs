﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public static class Enums
    {
        public enum TipoSesso
        {
            NonSpecificato = 0,
            Maschio = 1,
            Femmina = 2
        }

        public enum TipologiaGara
        {
            Italiana = 1,
            DoppioTurnoPrincipale = 2,
            DoppioTurnoRecupero = 3
        }

        public enum StatoCombattimento
        {
            InAttesa = 0,
            InCorso = 1,
            Concluso = 2
        }

    }
}
