﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class TrofeoInfo
    {

        public Guid IdGuid { get; set; }
        public string Titolo { get; set; }
        public DateTime DataEvento { get; set; }
        public string DataEventoStr { get; set; }

        public int TempoImNO { get; set; }
        public int TempoYuko { get; set; }
        public int TempoWari { get; set; }
        public int TempoIppon { get; set; }

        public int NumeroAtletiPartecipanti { get; set; }
        public int NumeroSocietaPartecipanti { get; set; }
        public int NumeroCategoriePartecipanti { get; set; }

        public int Id { get; set; }
    }
}
