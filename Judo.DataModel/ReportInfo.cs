﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Judo.DataModel
{
    public class ReportInfo
    {
        public Guid IdGuid { get; set; }
        public Enums.StatoCombattimento Stato { get; set; }
        public string CodicePosizioneAtleta { get; set; }
        public Enums.TipologiaGara TipologiaGara { get; set; }
        public int NumeroLivelliGara { get; set; }
        public int NumeroTatami { get; set; }

        public int Id { get; set; }

        public string NomeAtleta { get; set; }
        public string CognomeAtleta { get; set; }

        public Guid IDAtleta { get; set; }
        public Guid IDCategoria { get; set; }
        public Guid IDSocieta { get; set; }

        public Guid TrofeoGuid { get; set; }

        public string CodiceUnivoco { get; set; }

        public bool InternalVincitore { get; set; }
        public int InternalPosizione { get; set; }
    }
}
