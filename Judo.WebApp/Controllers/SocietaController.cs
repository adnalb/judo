﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Judo.DataLayer;
using Judo.DataModel;

namespace Judo.WebApp.Controllers
{
    [RoutePrefix("api/societa")]
    public class SocietaController : ApiController
    {
        private DataAccess dataAccess = new DataAccess();

        public async Task<IHttpActionResult> GetList()
        {

            List<SocietaInfo> elencoSocieta = new List<SocietaInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var societas = dbContext.SocietaSet.Where(s => s.TrofeoGuid == trofeo.IdGuid).ToList();

                    foreach (var societa in societas)
                    {
                        var societaInfo = new SocietaInfo();

                        societaInfo.IdGuid = societa.IdGuid;
                        societaInfo.ID = societa.Id;
                        societaInfo.Classifica = societa.Classifica;
                        societaInfo.Nazione = societa.Nazione;
                        societaInfo.Nome = societa.Nome;
                        societaInfo.NumeroAtleti = societa.ElencoAtleti.Count;
                        societaInfo.PunteggioTotale = societa.PunteggioTotale;
   

                        elencoSocieta.Add(societaInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoSocieta.OrderBy(s => s.Nome));

        }
    }
}
