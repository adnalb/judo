﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Judo.DataLayer;
using Judo.DataModel;

namespace Judo.WebApp.Controllers
{
    [RoutePrefix("api/trofeo")]
    public class TrofeoController : ApiController
    {
        private DataAccess dataAccess = new DataAccess();

        public enum TrofeoRetrieveChoiche
        {
            Last = 0,
            ByEventDate = 1,
            ByEventName = 2
        }


        [Route("")]
        public async Task<IHttpActionResult> Get()
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = "Trofeo..";

            try
            {
                infoTrofeo = GetCurrentTrofeo();
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(infoTrofeo);
        }

        [Route("all")]
        public async Task<IHttpActionResult> GetList()
        {

            List<TrofeoInfo> elencoTrofei = new List<TrofeoInfo>();

            try
            {
                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    // ritorno lista dei trofei ordinato per il piu recente
                    var trofeoList = dbContext.TrofeoConfigSet.ToList().OrderByDescending(t => t.DataEvento);

                    foreach (var dbTrofeo in trofeoList)
                    {
                        elencoTrofei.Add(FillInfoTrofeoData(dbTrofeo));
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoTrofei);

        }

        [Route("{trofeoID}")]
        public async Task<IHttpActionResult> GetById(Guid trofeoID)
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.IdGuid = trofeoID;

            infoTrofeo.Titolo = "Trofeo..";

            try
            {
                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var dbTrofeo = dbContext.TrofeoConfigSet.Where(t => t.IdGuid == trofeoID).FirstOrDefault();

                    if (dbTrofeo != null)
                    {
                        infoTrofeo = FillInfoTrofeoData(dbTrofeo);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(infoTrofeo);
        }

        [Route("trofeo/{titolo}")]
        public async Task<IHttpActionResult> GetByTitle(string titolo)
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = titolo;

            try
            {
                infoTrofeo = GetTrofeoByName(titolo);
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(infoTrofeo);
        }

        public TrofeoInfo GetCurrentTrofeo()
        {
            TrofeoInfo trofeoInfo = null;

            TrofeoRetrieveChoiche currentChoiche = GetSettingsCurrentTrofeoRetrieveChoiche();

            switch (currentChoiche)
            {
                case TrofeoRetrieveChoiche.ByEventDate:
                    trofeoInfo = GetTrofeoByEventDate(Properties.Settings.Default.TrofeoDate);
                    break;
                case TrofeoRetrieveChoiche.ByEventName:
                    trofeoInfo = GetTrofeoByName(Properties.Settings.Default.TrofeoName);
                    break;
                default:
                    trofeoInfo = GetLastTrofeo();
                    break;
            }

            return trofeoInfo;
        }

        private TrofeoRetrieveChoiche GetSettingsCurrentTrofeoRetrieveChoiche()
        {
            TrofeoRetrieveChoiche currentChoiche = TrofeoRetrieveChoiche.Last;

            if (Enum.IsDefined(typeof(TrofeoRetrieveChoiche), Properties.Settings.Default.TrofeoRetrieveChoiche))
            {
                currentChoiche = (TrofeoRetrieveChoiche)Properties.Settings.Default.TrofeoRetrieveChoiche;
            }

            return currentChoiche;
        }

        public TrofeoInfo GetLastTrofeo()
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = "Trofeo..";

            using (var dbContext = dataAccess.JudoModelContainerDbContext)
            {
                // by default get last inserted trofeo

                var dbTrofeo = dbContext.TrofeoConfigSet.OrderByDescending(t => t.DataEvento).FirstOrDefault();
                if (dbTrofeo != null)
                {
                    infoTrofeo = FillInfoTrofeoData(dbTrofeo);
                }
            }

            return infoTrofeo;
        }

        public TrofeoInfo GetTrofeoByName(string titolo)
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = titolo;

            using (var dbContext = dataAccess.JudoModelContainerDbContext)
            {
                var dbTrofeo = dbContext.TrofeoConfigSet.Where(t => t.Titolo.ToLower().Contains(titolo.ToLower())).FirstOrDefault();
                if (dbTrofeo != null)
                {
                    infoTrofeo = FillInfoTrofeoData(dbTrofeo);
                }
            }

            return infoTrofeo;
        }

        public TrofeoInfo GetTrofeoByEventDate(DateTime trofeoDate)
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = string.Format("Trofeo del {0}", trofeoDate.ToShortDateString());

            using (var dbContext = dataAccess.JudoModelContainerDbContext)
            {
                var dbTrofeo = dbContext.TrofeoConfigSet.Where(t => t.DataEvento.Year == trofeoDate.Year && t.DataEvento.Month == trofeoDate.Month && t.DataEvento.Day == trofeoDate.Day).FirstOrDefault();
                if (dbTrofeo != null)
                {
                    infoTrofeo = FillInfoTrofeoData(dbTrofeo);
                }
            }

            return infoTrofeo;
        }

        public TrofeoInfo GetTrofeoByEventGuid(Guid trofeoGuid)
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = string.Format("Trofeo ID {0}", trofeoGuid);

            using (var dbContext = dataAccess.JudoModelContainerDbContext)
            {
                var dbTrofeo = dbContext.TrofeoConfigSet.Where(t => t.IdGuid == trofeoGuid).FirstOrDefault();
                if (dbTrofeo != null)
                {
                    infoTrofeo = FillInfoTrofeoData(dbTrofeo);
                }
            }

            return infoTrofeo;
        }


        private TrofeoInfo FillInfoTrofeoData(TrofeoConfig dbTrofeo)
        {
            TrofeoInfo infoTrofeo = new TrofeoInfo();

            infoTrofeo.Titolo = "Trofeo..";

            if (dbTrofeo != null)
            {
                infoTrofeo.IdGuid = dbTrofeo.IdGuid;
                infoTrofeo.Titolo = dbTrofeo.Titolo;
                //infoTrofeo.DataEventoStr = dbTrofeo.DataEventoStr;
                infoTrofeo.DataEventoStr = TransformTrofeoDataEvento(dbTrofeo.DataEvento, dbTrofeo.DataEventoStr);
                infoTrofeo.DataEvento = dbTrofeo.DataEvento;
                infoTrofeo.TempoImNO = dbTrofeo.TempoImNO;
                infoTrofeo.TempoIppon = dbTrofeo.TempoIppon;
                infoTrofeo.TempoWari = dbTrofeo.TempoWari;
                infoTrofeo.TempoYuko = dbTrofeo.TempoYuko;

                infoTrofeo.NumeroAtletiPartecipanti = dbTrofeo.NumeroAtletiPartecipanti;
                infoTrofeo.NumeroCategoriePartecipanti = dbTrofeo.NumeroCategoriePartecipanti;
                infoTrofeo.NumeroSocietaPartecipanti = dbTrofeo.NumeroSocietaPartecipanti;
            }

            return infoTrofeo;
        }

        private string TransformTrofeoDataEvento(DateTime dataEvento, string dataEventoStr)
        {
            try
            {
                int giorno = dataEvento.Day;
                int giorniInMese = DateTime.DaysInMonth(dataEvento.Year, dataEvento.Month);
                if ((giorno + 1) <= giorniInMese)
                {
                    var dataEventoSplit = dataEventoStr.Split(' ');
                    return string.Format("{0}/{1} {2} {3}", giorno, (giorno + 1), dataEventoSplit[1], dataEventoSplit[2]);
                }
            }
            catch (Exception) { }

            return dataEventoStr;
        }
    }
}
