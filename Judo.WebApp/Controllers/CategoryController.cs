﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Judo.DataLayer;
using Judo.DataModel;

namespace Judo.WebApp.Controllers
{
    [RoutePrefix("api/category")]
    public class CategoryController : ApiController
    {
        private DataAccess dataAccess = new DataAccess();

        [Route("")]
        public async Task<IHttpActionResult> GetList()
        {

            List<CategoriaInfo> elencoCategorie = new List<CategoriaInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    elencoCategorie = GetCategoryListForTrofeo(dbContext, trofeo.IdGuid);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoCategorie);

        }

        [Route("{trofeoID}")]
        public async Task<IHttpActionResult> GetListForTrofeo(Guid trofeoID)
        {

            List<CategoriaInfo> elencoCategorie = new List<CategoriaInfo>();

            try
            {
                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    elencoCategorie = GetCategoryListForTrofeo(dbContext, trofeoID);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoCategorie);

        }

        [Route("catid/{catID}")]
        public async Task<IHttpActionResult> GetCategoryByID(Guid catID)
        {
            var categoriaInfo = new CategoriaInfo();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var categoria = dbContext.CategoriaSet.Where(c => c.IdGuid == catID && c.TrofeoGuid == trofeo.IdGuid).FirstOrDefault();

                    FillCategoria(categoria, categoriaInfo);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(categoriaInfo);

        }


        private List<CategoriaInfo> GetCategoryListForTrofeo(JudoModelContainer dbContext, Guid trofeoID)
        {
            List<CategoriaInfo> elencoCategorie = new List<CategoriaInfo>();

            var categorie = dbContext.CategoriaSet.Where(c => c.TrofeoGuid == trofeoID).ToList();

            foreach (var categoria in categorie)
            {
                var categoriaInfo = new CategoriaInfo();

                FillCategoria(categoria, categoriaInfo);

                elencoCategorie.Add(categoriaInfo);
            }

            return elencoCategorie.OrderBy(c => c.Codice).ToList();
        }

        private void FillCategoria(Categoria categoria, CategoriaInfo categoriaInfo)
        {
            categoriaInfo.IdGuid = categoria.IdGuid;
            categoriaInfo.Codice = categoria.Codice;
            categoriaInfo.Descrizione = categoria.Descrizione;
            categoriaInfo.DurataExtraGara = categoria.DurataExtraGara;
            categoriaInfo.DurataGara = categoria.DurataGara;
            categoriaInfo.Eta = categoria.Eta;
            categoriaInfo.Id = categoria.Id;
            categoriaInfo.NumeroTatami = categoria.NumeroTatami;
            categoriaInfo.Peso = categoria.Peso;
            categoriaInfo.Sesso = (Enums.TipoSesso)Enum.Parse(typeof(Enums.TipoSesso), categoria.Sesso);
            categoriaInfo.Stato = (Enums.StatoCombattimento)((int)categoria.Stato);
            categoriaInfo.TipoGara = (Enums.TipologiaGara)((int)categoria.TipologiaGara);

            categoriaInfo.NumeroAtletiPartecipanti = categoria.ElencoAtleti.Count;
        }

    }


}
