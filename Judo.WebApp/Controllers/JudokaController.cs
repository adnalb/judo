﻿using Judo.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Judo.DataLayer;

namespace Judo.WebApp.Controllers
{
    [RoutePrefix("api/judoka")]
    public class JudokaController : ApiController
    {
        private DataAccess dataAccess = new DataAccess();

        [HttpGet]
        [Route("")]
        public async Task<IHttpActionResult> GetList()
        {
            List<AtletaInfo> elencoAtleti = new List<AtletaInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();
                
                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var atleti = dbContext.AtletaSet.Where(a => a.TrofeoGuid == trofeo.IdGuid).ToList();

                    foreach (var atleta in atleti)
                    {

                        var atletaInfo = new AtletaInfo();

                        atletaInfo.IdGuid = atleta.IdGuid;
                        atletaInfo.ID = atleta.Id;
                        atletaInfo.Anno = atleta.Anno;
                        atletaInfo.Codice = atleta.Codice;
                        atletaInfo.Cognome = atleta.Cognome;
                        atletaInfo.Nome = atleta.Nome;
                        atletaInfo.Ranking = atleta.Ranking;
                        atletaInfo.Sesso = (Enums.TipoSesso)((int)atleta.Sesso);
                        atletaInfo.Peso = atleta.ElencoCategorie.First().Peso;
                        atletaInfo.Eta = atleta.ElencoCategorie.First().Eta;
                        atletaInfo.DescrizioneCategoria = atleta.ElencoCategorie.First().Descrizione;
                        atletaInfo.NomeSocieta = atleta.Societa.Nome;

                        atletaInfo.IDCategoria = atleta.ElencoCategorie.First().IdGuid;
                        atletaInfo.IDSocieta = atleta.Societa.IdGuid;

                        AggiungiCategorieAtleta(atletaInfo, atleta);

                        elencoAtleti.Add(atletaInfo);
                    }

                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoAtleti.OrderBy(a => a.Cognome));
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> Get(int id)
        {
            return Ok();

        }

        [Route("societa/{nome}")]
        public async Task<IHttpActionResult> GetAtletiSocieta(string nome)
        {
            List<AtletaInfo> elencoAtleti = new List<AtletaInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var atleti = dbContext.AtletaSet.Where(a => a.TrofeoGuid == trofeo.IdGuid && a.Societa.Nome == nome).ToList();

                    foreach (var atleta in atleti)
                    {

                        var atletaInfo = new AtletaInfo();

                        atletaInfo.IdGuid = atleta.IdGuid;
                        atletaInfo.ID = atleta.Id;
                        atletaInfo.Anno = atleta.Anno;
                        atletaInfo.Codice = atleta.Codice;
                        atletaInfo.Cognome = atleta.Cognome;
                        atletaInfo.Nome = atleta.Nome;
                        atletaInfo.Ranking = atleta.Ranking;
                        atletaInfo.Sesso = (Enums.TipoSesso)((int)atleta.Sesso);
                        atletaInfo.Peso = atleta.ElencoCategorie.First().Peso;
                        atletaInfo.Eta = atleta.ElencoCategorie.First().Eta;
                        atletaInfo.DescrizioneCategoria = atleta.ElencoCategorie.First().Descrizione;
                        atletaInfo.NomeSocieta = atleta.Societa.Nome;

                        atletaInfo.IDCategoria = atleta.ElencoCategorie.First().IdGuid;
                        atletaInfo.IDSocieta = atleta.Societa.IdGuid;

                        AggiungiCategorieAtleta(atletaInfo, atleta);

                        elencoAtleti.Add(atletaInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoAtleti.OrderBy(a => a.Cognome));

        }

        [Route("categoria/{id}")]
        public async Task<IHttpActionResult> GetAtletiCategoria(int id)
        {
            List<AtletaInfo> elencoAtleti = new List<AtletaInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var categoria = dbContext.CategoriaSet.Single(c => c.Id == id && c.TrofeoGuid == trofeo.IdGuid);
                    var classificaAtleti = dbContext.ClassificaCategoriaSet.Where(c => c.CategoriaId == categoria.IdGuid && c.TrofeoGuid == trofeo.IdGuid);

                    foreach (var cAtleta in classificaAtleti)
                    {
                        var atletaInfo = new AtletaInfo();

                        var atleta = dbContext.AtletaSet.FirstOrDefault(a => a.IdGuid == cAtleta.AtletaId);
                        if (atleta != null)
                        {
                            atletaInfo.IdGuid = atleta.IdGuid;
                            atletaInfo.ID = atleta.Id;
                            atletaInfo.Anno = atleta.Anno;
                            atletaInfo.Codice = atleta.Codice;
                            atletaInfo.Cognome = atleta.Cognome;
                            atletaInfo.Nome = atleta.Nome;
                            atletaInfo.Ranking = cAtleta.Posizione;
                            atletaInfo.Sesso = (Enums.TipoSesso)((int)atleta.Sesso);
                            atletaInfo.Peso = categoria.Peso;
                            atletaInfo.Eta = categoria.Eta;
                            atletaInfo.DescrizioneCategoria = categoria.Descrizione;
                            atletaInfo.NomeSocieta = atleta.Societa.Nome;

                            atletaInfo.IDCategoria = categoria.IdGuid;
                            atletaInfo.IDSocieta = atleta.Societa.IdGuid;
                        }
                        else
                        {
                            atletaInfo.Cognome = "NON ";
                            atletaInfo.Nome = "TROVATO";
                            atletaInfo.Ranking = 999;
                        }
                        elencoAtleti.Add(atletaInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoAtleti.OrderBy(a => a.Cognome));

        }




        private void AggiungiCategorieAtleta(AtletaInfo atletaInfo, Atleta atleta)
        {
            atletaInfo.ElencoCategorie = new List<CategoriaInfo>();

            foreach (var cat in atleta.ElencoCategorie)
            {
                CategoriaInfo categoria = new CategoriaInfo();

                categoria.IdGuid = cat.IdGuid;
                categoria.Codice = cat.Codice;
                categoria.Descrizione = cat.Descrizione;
                categoria.Sesso = (Enums.TipoSesso)(int.Parse(cat.Sesso));
                categoria.Peso = cat.Peso;
                categoria.Eta = cat.Eta;

                categoria.NumeroTatami = cat.NumeroTatami;
                categoria.NumeroAtletiPartecipanti = cat.ElencoAtleti.Count;
                categoria.DurataGara = cat.DurataGara;
                categoria.DurataExtraGara = cat.DurataExtraGara;
                categoria.Stato = (Enums.StatoCombattimento)((int)cat.Stato);
                categoria.TipoGara = (Enums.TipologiaGara)((int)cat.TipologiaGara);

                categoria.TrofeoGuid = cat.Trofeo.IdGuid;

                categoria.Id = cat.Id;

                atletaInfo.ElencoCategorie.Add(categoria);
            }
        }
    }
}
