﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Judo.DataLayer;
using Judo.DataModel;

namespace Judo.WebApp.Controllers
{
    [RoutePrefix("api/bracket")]
    public class BracketController : ApiController
    {
        private DataAccess dataAccess = new DataAccess();

        public async Task<IHttpActionResult> Get()
        {
            return Ok("1");
        }

        [HttpGet]
        [Route("cat/{id}/{tipo}")]
        public async Task<IHttpActionResult> FindReports(int id, int tipo)
        {
            List<ReportInfo> elencoReports = new List<ReportInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                TipologiaGara tipoGara = (TipologiaGara)tipo;

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var categoria = dbContext.CategoriaSet.Where(c => c.Id == id && c.TrofeoGuid == trofeo.IdGuid).FirstOrDefault();

                    if (categoria == null)
                        return BadRequest("Categoria non trovata");

                    var reports = dbContext.ReportSet.Where(r => r.CategoriaId == categoria.Id && r.TipologiaGara == tipoGara && r.TrofeoGuid == trofeo.IdGuid).ToList();

                    foreach (var report in reports)
                    {

                        var reportInfo = new ReportInfo();

                        reportInfo.IdGuid = report.IdGuid;
                        reportInfo.CodicePosizioneAtleta = report.CodicePosizioneAtleta;
                        reportInfo.Id = report.Id;
                        reportInfo.NumeroLivelliGara = report.NumeroLivelliGara;
                        reportInfo.NomeAtleta = report.Atleta.Nome;
                        reportInfo.CognomeAtleta = report.Atleta.Cognome;
                        reportInfo.NumeroTatami = categoria.NumeroTatami;

                        reportInfo.IDAtleta = report.Atleta.IdGuid;
                        reportInfo.IDCategoria = categoria.IdGuid;
                        reportInfo.IDSocieta = report.Atleta.Societa.IdGuid;

                        reportInfo.Stato = (Enums.StatoCombattimento)((int)categoria.Stato);
                        reportInfo.TipologiaGara = (Enums.TipologiaGara)((int)tipoGara);

                        elencoReports.Add(reportInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoReports);
        }
    }
}
