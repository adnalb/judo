﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Judo.DataLayer;
using Judo.DataModel;

namespace Judo.WebApp.Controllers
{
    [RoutePrefix("api/tatami")]
    public class TatamiController : ApiController
    {
        private DataAccess dataAccess = new DataAccess();

        public async Task<IHttpActionResult> GetList()
        {
            List<TatamiInfo> elencoTatami = new List<TatamiInfo>();

            try
            {
                TrofeoInfo trofeo = new TrofeoController().GetCurrentTrofeo();

                using (var dbContext = dataAccess.JudoModelContainerDbContext)
                {
                    var categorie = dbContext.CategoriaSet.Where(c => c.TrofeoGuid == trofeo.IdGuid).ToList();

                    foreach (var cat in categorie)
                    {
                        var catInfo = new CategoriaInfo();
                        catInfo.Codice = cat.Codice;
                        catInfo.Descrizione = cat.Descrizione + "(" + cat.Sesso + ")";
                        catInfo.Sesso =  (cat.Sesso == "1" || cat.Sesso == "Maschio") ? Enums.TipoSesso.Maschio : (cat.Sesso == "2" || cat.Sesso == "Femmina" )? Enums.TipoSesso.Femmina : Enums.TipoSesso.NonSpecificato;
                        catInfo.Peso = cat.Peso;
                        catInfo.Eta = cat.Eta;
                        catInfo.DurataGara = cat.DurataGara;
                        catInfo.DurataExtraGara = cat.DurataExtraGara;

                        TatamiInfo tatami = null;

                        if (elencoTatami.Count(t => t.NumeroTatami == cat.NumeroTatami) == 0)
                        {
                            tatami = new TatamiInfo()
                            {
                                NumeroTatami = cat.NumeroTatami,
                                ElencoCategorie = new List<CategoriaInfo>()
                            };

                            elencoTatami.Add(tatami);
                        }
                        
                        tatami = elencoTatami.Where(t => t.NumeroTatami == cat.NumeroTatami).FirstOrDefault();

                        if (cat.Stato == StatoCombattimento.InCorso)
                        {
                            tatami.CategoriaInCorso = catInfo;
                        }

                        if(tatami.ElencoCategorie.Count(c => c.IdGuid == cat.IdGuid) == 0)
                        {
                            tatami.ElencoCategorie.Add(catInfo);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(string.Format("{0}: {1} - {2}", this, ex.Message, ex));
            }

            return Ok(elencoTatami);
        }
    }
}
