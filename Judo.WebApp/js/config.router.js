'use strict';

/**
 * Config for the router
 */
angular.module('app')
  .run(
    [          '$rootScope', '$state', '$stateParams',
      function ($rootScope,   $state,   $stateParams) {
          $rootScope.$state = $state;
          $rootScope.$stateParams = $stateParams;        
      }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider',
      function ($stateProvider,   $urlRouterProvider) {
          
          $urlRouterProvider
              .otherwise('/judo/index');
          $stateProvider
              .state('app', {
                  abstract: true,
                  url: '/app',
                  templateUrl: 'tpl/app.html'
              })
              
              .state('judo', {
                  abstract: true,
                  url: '/judo',
                  templateUrl: 'views/judo.html'
              })
              .state('judo.index', {
                  url: '/index',
                  templateUrl: 'views/index.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                      function( $ocLazyLoad ){
                        return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
                  }
              })
              .state('judo.bracket', {
                  url: '/bracket',
                  templateUrl: 'views/bracket.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                      function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
                  }
              })
              .state('judo.tatami', {
                  url: '/tatami',
                  templateUrl: 'views/tatami.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                      function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
                  }
              })
              .state('judo.team', {
                  url: '/team',
                  templateUrl: 'views/team.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                      function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
                  }
              })
              .state('judo.judoka', {
                  url: '/judoka',
                  templateUrl: 'views/judoka.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                      function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
                  }
              })
              .state('judo.ranking', {
                  url: '/ranking',
                  templateUrl: 'views/ranking.html',
                  resolve: {
                    deps: ['$ocLazyLoad',
                      function( $ocLazyLoad ){
                          return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
                  }
              })
          .state('judo.judokasocieta', {
              url: '/judokasocieta/{teamId}',
              templateUrl: 'views/judokasocieta.html',
              resolve: {
                  deps: ['$ocLazyLoad',
                    function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['js/controllers/judo.js?v=2']);
                    }]
              }
          })
      }
    ]
  );