'use strict';

var controllerUrl = '';

/* Controllers */

// Category / Bracket controller 
app.controller('BracketController', ['$http', '$localStorage', '$location', '$scope', function ($http, $localStorage, $location, $scope) {
    $scope.tabellone = 1;
    $scope.bracket = [];
    // Bracket controller 

    $scope.refresh = function () {
        console.info('---------------------------');
        $scope.bracket = [];
        //console.info('$localStorage.selectedYear', $localStorage.selectedYear);
        //console.info('$localStorage.selectedSex', $localStorage.selectedSex);
        //console.info('$localStorage.selectedWeight', $localStorage.selectedWeight);
        //console.info('$scope.selectedCategory.tipoGara ', $scope.selectedCategory.tipoGara);
        if ($scope.selectedCategory.tipoGara == 1) {
            console.info('---------------------------');
            console.info('ITALIANA');
            $http.get(controllerUrl + '/api/bracket/cat/' + $scope.selectedCategory.id + '/1').success(function (response) {
                //console.info('api/report', response);
                $scope.apiBracket = response;
                $scope.bracket = [];
                angular.forEach($scope.apiBracket, function (item, index) {
                    $scope.bracket.push({ position: item.codicePosizioneAtleta, judoka: item });
                    console.info('main -->', { position: item.codicePosizioneAtleta, judoka: item });
                });
            }).error(function (msg, code) {
                // body...
                console.info('error: ', msg)
            });
        }

        if ($scope.selectedCategory.tipoGara != 1) {
            console.info('---------------------------');
            console.info('PRINCIPALE');
            $http.get(controllerUrl + '/api/bracket/cat/' + $scope.selectedCategory.id + '/2').success(function (response) {
                //console.info('api/report', response);
                $scope.apiBracket = response;
                $scope.bracket = [];
                angular.forEach($scope.apiBracket, function (item, index) {
                    $scope.bracket.push({ position: item.codicePosizioneAtleta, judoka: item });
                    console.info('main -->', { position: item.codicePosizioneAtleta, judoka: item });
                });
            }).error(function (msg, code) {
                // body...
                console.info('error: ', msg)
            });
            console.info('---------------------------');
            console.info('RECUPERI');
            $http.get(controllerUrl + '/api/bracket/cat/' + $scope.selectedCategory.id + '/3').success(function (response) {
                console.info('api/report', response);
                $scope.apiBracketRec = response;
                $scope.bracketRec = [];
                angular.forEach($scope.apiBracketRec, function (item, index) {
                    $scope.bracketRec.push({ position: item.codicePosizioneAtleta, judoka: item });
                    console.info('rec  -->', { position: item.codicePosizioneAtleta, judoka: item });

                });
                console.info("rec count", $scope.bracketRec.lenght)
            }).error(function (msg, code) {
                // body...
                console.info('error: ', msg)
            });
        }
      }

      $scope.selectSex = function (sexId, cleanData) {
        console.info(sexId);
        $scope.selectedSex = sexId;

        $localStorage.selectedSex = sexId;
        if (cleanData != false)
        {
            $localStorage.selectedWeight = null;
            $localStorage.selectedYear = null;
        }

        $scope.yearCategories = [];
        $scope.weightCategories = [];
        $scope.selectedYear = null;
        $scope.selectedWeight = null;
        $scope.selectedCategory = null;
        angular.forEach($scope.judoCategories, function (item, index) {
          var year = item.eta;
          if(item.sesso == $scope.selectedSex && $scope.yearCategories.indexOf(item.eta) < 0) {
              $scope.yearCategories.push(item.eta);
          }
        });
        console.info('$scope.yearCategories', $scope.yearCategories);
        $scope.bracket = [];
      };

      $scope.selectYear = function (year, cleanData) {
        console.info("selected year", year);
        $scope.selectedYear = year;

        $localStorage.selectedYear = year;

        if (cleanData != false) {
            $localStorage.selectedWeight = null;
        }

        $scope.weightCategories = [];
        $scope.selectedWeight = null;
        $scope.selectedCategory = null;
        angular.forEach($scope.judoCategories, function (item, index) {
          //var year = item.eta;
          if(item.sesso == $scope.selectedSex && item.eta == year && $scope.weightCategories.indexOf(item.peso) < 0) {
              $scope.weightCategories.push(item.peso);
          }
        });
        $scope.bracket = [];
      };

      $scope.selectWeight = function (weight, cleanData) {
          $scope.selectedWeight = weight;

          $localStorage.selectedWeight = weight;

        angular.forEach($scope.judoCategories, function (item, index) {
          if(item.sesso == $scope.selectedSex && item.eta == $scope.selectedYear && item.peso == $scope.selectedWeight) {
              console.info('selectedCategory', item);
              $scope.bracket = [];
                $scope.selectedCategory = item;
                $scope.refresh();
            
          }
        });
        
      };

      $scope.gotoTatami= function (numeroTatami) {
          $localStorage.selectedNumeroTatami = numeroTatami;
          $location.path('judo/tatami');
      };

      // Category controller 

      $http.get(controllerUrl + '/api/category').success(function (response) {
        $scope.judoCategories = response;
        //console.info('/api/category - ' + response);
        //console.info('/api/category - selectedSex: ' + $localStorage.selectedSex);
        //console.info('/api/category - selectedYear: ' + $localStorage.selectedYear);
        //console.info('/api/category - selectedWeight: ' + $localStorage.selectedWeight);


        if($localStorage.selectedSex != null) {
          $scope.selectSex($localStorage.selectedSex, false);
        }
        if ($localStorage.selectedYear != null) {
            $scope.selectYear($localStorage.selectedYear, false);
        }
        if ($localStorage.selectedWeight != null) {
            $scope.selectWeight($localStorage.selectedWeight, false);
        }


        //console.info('$scope.judoCategories', $scope.judoCategories);
        //console.info('$scope.judoCategories[0]', $scope.judoCategories[0]);
        //console.info('$scope.judoCategories[0].IdGuid', $scope.judoCategories[0].idGuid);
      }).error(function (msg, code) {
        console.info(msg);
      });


      $scope.getPositionIta = function (argument) {
          var returnValue = null;
          console.info('getPositionIta', $scope.bracket)
          angular.forEach($scope.bracket, function (item, index) {
              if (item.position == argument) {
                  returnValue = item.judoka.cognomeAtleta + ' ' + item.judoka.nomeAtleta;
              }
          });
          return returnValue;
      };

      $scope.getPositionPrin = function (argument) {
          var returnValue = null;
          console.info('getPositionPrin', $scope.bracket)
        angular.forEach($scope.bracket, function (item, index) {
          if(item.position == argument) {
            returnValue = item.judoka.cognomeAtleta + ' ' + item.judoka.nomeAtleta;
          }
        });
        return returnValue;    
      };

      $scope.getPositionRec = function (argument) {
          var returnValue = null;
          console.info('getPositionRec', $scope.bracket)
        angular.forEach($scope.bracketRec, function (item, index) {
          if(item.position == argument) {
            returnValue = item.judoka.cognomeAtleta + ' ' + item.judoka.nomeAtleta;
          }
        });
        return returnValue;    
      };

      $scope.gotoRanking = function (cat) {
          $localStorage.selectedSex = cat.sesso;
          $localStorage.selectedYear = cat.eta;
          $localStorage.selectedWeight = cat.peso;
          $location.path('judo/ranking');
      }

    }]);

// Ranking Controller 
app.controller('RankingController', ['$http', '$scope', '$localStorage', '$location', function ($http, $scope, $localStorage, $location) {

    $scope.rankWeightCategories = [];
    $scope.rankYearCategories = [];
    $scope.judokas = [];

    $scope.refresh = function () {
        console.info('$scope.rankSelectedCategory', $scope.rankSelectedCategory);

        $http.get(controllerUrl + '/api/judoka/categoria/' + $scope.rankSelectedCategory.id).success(function (response) {

            if ($scope.judokas) {
                $scope.judokas.splice(0, $scope.judokas.length);
            }
            $scope.judokaWinner = response[0];

            console.info('---------------------------');
            console.info('CLASSIFICA JUDOKA');

            angular.forEach(response, function (judokaItem, judokaIndex) {

                console.info('judokaItem', judokaItem);

                if ($scope.judokaWinner.ranking > judokaItem.ranking) {
                    $scope.judokaWinner = judokaItem;
                }
                console.info('$scope.judokaWinner', $scope.judokaWinner);

                //console.info('judokaItem.sesso', judokaItem.sesso);
                //console.info('judokaItem.eta', judokaItem.eta);
                //console.info('judokaItem.peso', judokaItem.peso);
                //console.info('judokaItem.ranking', judokaItem.ranking);

                if (judokaItem.sesso == $scope.rankSelectedSex && judokaItem.eta == $scope.rankSelectedYear && judokaItem.peso == $scope.rankSelectedWeight) {
                    $scope.judokas.push(judokaItem);
                }
            });
            console.info($scope.judokas);

            console.info('$scope.judokaWinner', $scope.judokaWinner);
        }).error(function (msg, code) {
            // body...
            console.info('error: ', msg)
        });

        console.info('---------------------------');
    };

    $scope.rankSelectSex = function (sexId, cleanData) {

        if ($scope.judokas) {
            $scope.judokas.splice(0, $scope.judokas.length);
        }
        if ($scope.rankWeightCategories) {
            $scope.rankWeightCategories.splice(0, $scope.rankWeightCategories.length);
        }
        if ($scope.rankYearCategories) {
            $scope.rankYearCategories.splice(0, $scope.rankYearCategories.length);
        }

        console.info(sexId);
        $scope.rankSelectedSex = sexId;

        $localStorage.selectedSex = sexId;
        if (cleanData != false) {
            $localStorage.selectedWeight = null;
            $localStorage.selectedYear = null;
        }


        //$scope.rankYearCategories = [];
        //$scope.rankWeightCategories = [];
        $scope.rankSelectedYear = '';
        $scope.rankSelectedWeight = '';
        $scope.rankSelectedCategory = '';
        $scope.judokaWinner = null;
        
        angular.forEach($scope.judoCategories, function (item, index) {
            var year = item.eta;
            if (item.sesso == $scope.rankSelectedSex && $scope.rankYearCategories.indexOf(item.eta) < 0) {
                $scope.rankYearCategories.push(item.eta);
            }
        });
        console.info('$scope.rankYearCategories', $scope.rankYearCategories);
        
    };

    $scope.rankSelectYear = function (year, cleanData) {


        if ($scope.judokas) {
            $scope.judokas.splice(0, $scope.judokas.length);
        }
        if ($scope.rankWeightCategories) {
            $scope.rankWeightCategories.splice(0, $scope.rankWeightCategories.length);
        }

        console.info("selected year", year);
        $scope.rankSelectedYear = year;

        $localStorage.selectedYear = year;

        if (cleanData != false) {
            $localStorage.selectedWeight = null;
        }

        $scope.rankSelectedCategory = '';
        $scope.rankSelectedWeight = '';
        $scope.judokaWinner = null;

        angular.forEach($scope.judoCategories, function (item, index) {
            //var year = item.eta;
            if (item.sesso == $scope.rankSelectedSex && item.eta == $scope.rankSelectedYear && $scope.rankWeightCategories.indexOf(item.peso) < 0) {
                $scope.rankWeightCategories.push(item.peso);
            }
        });
    };

    $scope.rankSelectWeight = function (weight, cleanData) {
        $scope.rankSelectedWeight = weight;

        $localStorage.selectedWeight = weight;

        if ($scope.judokas) {
            $scope.judokas.splice(0, $scope.judokas.length);
        }
        $scope.judokaWinner = null;

        angular.forEach($scope.judoCategories, function (item, index) {
            if (item.sesso == $scope.rankSelectedSex && item.eta == $scope.rankSelectedYear && item.peso == $scope.rankSelectedWeight) {
                console.info('selectedCategory', item);
                $scope.rankSelectedCategory = item;
                $scope.refresh();
            }
        });

    };


      // Category controller 

      $http.get(controllerUrl + '/api/category').success(function (response) {
          $scope.rankingType = 'rankCategories';
          $scope.judoCategories = response;

          if ($localStorage.selectedSex != null) {
              $scope.rankSelectSex($localStorage.selectedSex, false);
          }
          if ($localStorage.selectedYear != null) {
              $scope.rankSelectYear($localStorage.selectedYear, false);
          }
          if ($localStorage.selectedWeight != null) {
              $scope.rankSelectWeight($localStorage.selectedWeight, false);
          }

          //console.info('$scope.judoCategories', $scope.judoCategories);
          //console.info('$scope.judoCategories[0]', $scope.judoCategories[0]);
          //console.info('$scope.judoCategories[0].idGuid', $scope.judoCategories[0].idGuid);
      }).error(function (msg, code) {
          console.info(msg);
      });

      $scope.gotoBracket = function (cat) {
          $localStorage.selectedSex = cat.sesso;
          $localStorage.selectedYear = cat.eta;
          $localStorage.selectedWeight = cat.peso;
          $location.path('judo/bracket');
      }

  }]);

// Team Controller 
app.controller('TeamController', ['$http', '$scope', '$location', function($http, $scope, $location) {

    $http.get(controllerUrl + '/api/societa').success(function (response) {
        $scope.teams = response;
        console.info(response);
      }).error(function (msg, code) {
          // body...
          console.info('error: ', msg)
      });

      $scope.viewSocietaAtleti = function (team) {
          $location.path('/judo/judokasocieta/' + team.idGuid);
      }

    }]);

// Tatami Controller
app.controller('TatamiCtrl', ['$http', '$location', '$localStorage', '$scope', function ($http, $location, $localStorage, $scope) {

     $http.get(controllerUrl + '/api/tatami').success(function (response) {
        $scope.tatamies = response;
        console.info(response);

        $localStorage.selectedSex = null;
        $localStorage.selectedWeight = null;
        $localStorage.selectedYear = null;

        if ($localStorage.selectedNumeroTatami != null) {
            $scope.selectTatami($scope.getTatami($scope.tatamies, $localStorage.selectedNumeroTatami));
            $localStorage.selectedNumeroTatami = null;
        }

      }).error(function (msg, code) {
        console.info(msg);
      });

     $scope.getTatami = function (tatamies, numeroTatami) {
         if (tatamies != undefined) {
             var i;
             for (i = 0; i < tatamies.length; i++) {
                 if (tatamies[i].numeroTatami === numeroTatami) return tatamies[i];
             }
         }

         return undefined;
     };

     $scope.selectTatami = function (tatami) {
         if (tatami != undefined)
         {
             $scope.selectedTatami = tatami;
         }       
      };

      $scope.gotoBracket = function (cat) {
        $localStorage.selectedSex = cat.sesso;
        $localStorage.selectedYear = cat.eta;
        $localStorage.selectedWeight = cat.peso;
        $location.path('judo/bracket');
      }

      $scope.gotoRanking = function (cat) {
          $localStorage.selectedSex = cat.sesso;
          $localStorage.selectedYear = cat.eta;
          $localStorage.selectedWeight = cat.peso;
          $location.path('judo/ranking');
      }

    }]);

// Judoka Controller 
app.controller('JudokaCtrl', ['$http', '$location', '$localStorage', '$scope', function ($http, $location, $localStorage, $scope) {

    $scope.selectItem = function (item) {
        if($scope.item)
        {
            $scope.item.selected = false;
            
        }
        $scope.item = item;
        $scope.item.selected = true;

        $localStorage.selectedSex = null;
        $localStorage.selectedWeight = null;
        $localStorage.selectedYear = null;
      }

      $http.get(controllerUrl + '/api/judoka').success(function (response) {
        console.info(response);
        $scope.items = response;
        $scope.selectItem($scope.items[0]);

      }).error(function (msg, code) {
        console.info(msg);
      });
        
      $scope.viewSocietaAtletiByID = function (teamIdGuid) {
          console.info('viewSocietaAtletiByID', teamIdGuid);
          $location.path('/judo/judokasocieta/' + teamIdGuid);
      }

      $scope.gotoBracketByCatID = function (catIdGuid) {

          console.info('gotoBracketByCatID', catIdGuid);

          $http.get(controllerUrl + '/api/category/catid/' + catIdGuid).success(function (response) {
              $scope.judoCategory = response;

              console.info('$scope.judoCategory.IdGuid ', $scope.judoCategory.idGuid);
              console.info('$scope.judoCategory.sesso ', $scope.judoCategory.sesso);
              console.info('$scope.judoCategory.eta ', $scope.judoCategory.eta);
              console.info('$scope.judoCategory.peso ', $scope.judoCategory.peso);

              $localStorage.selectedSex = $scope.judoCategory.sesso;
              $localStorage.selectedYear = $scope.judoCategory.eta;
              $localStorage.selectedWeight = $scope.judoCategory.peso;
              $location.path('judo/bracket');

          }).error(function (msg, code) {
              console.info(msg);
          });
      }

    }]);

// Judoka Societa Controller 
app.controller('JudokaSocietaCtrl', ['$http', '$location', '$localStorage', '$scope', '$stateParams', function ($http, $location, $localStorage, $scope, $stateParams) {
        console.info($stateParams);

        $scope.selectItem = function (item) {
            if ($scope.item) {
                $scope.item.selected = false;

            }
            $scope.item = item;
            $scope.item.selected = true;

            $localStorage.selectedSex = null;
            $localStorage.selectedWeight = null;
            $localStorage.selectedYear = null;
        }

        $http.get(controllerUrl + '/api/judoka').success(function (response) {
            $scope.items = [];
            console.info(response);
            angular.forEach(response, function (item, index) {
                if (item.idSocieta == $stateParams.teamId)
                    $scope.items.push(item);
            })
            $scope.selectItem($scope.items[0]);
        }).error(function (msg, code) {
            console.info(msg);
        });

        $scope.gotoBracketByCatID = function (catIdGuid) {

            console.info('gotoBracketByCatID', catIdGuid);

            $http.get(controllerUrl + '/api/category/catid/' + catIdGuid).success(function (response) {
                $scope.judoCategory = response;

                console.info('$scope.judoCategory.IdGuid ', $scope.judoCategory.idGuid);
                console.info('$scope.judoCategory.sesso ', $scope.judoCategory.sesso);
                console.info('$scope.judoCategory.eta ', $scope.judoCategory.eta);
                console.info('$scope.judoCategory.peso ', $scope.judoCategory.peso);

                $localStorage.selectedSex = $scope.judoCategory.sesso;
                $localStorage.selectedYear = $scope.judoCategory.eta;
                $localStorage.selectedWeight = $scope.judoCategory.peso;
                $location.path('judo/bracket');

            }).error(function (msg, code) {
                console.info(msg);
            });
        }

    }]);

// Trofeo Controller 
app.controller('TrofeoController', ['$http', '$scope', '$localStorage', function ($http, $scope, $localStorage) {
    console.info("test");

        $scope.getData = function () {
            $http.get(controllerUrl + '/api/trofeo').success(function (response) {
                $scope.trofeo = response;
                console.info(response);

                $localStorage.selectedSex = null;
                $localStorage.selectedWeight = null;
                $localStorage.selectedYear = null;

            }).error(function (msg, code) {
                // body...
                console.info('error: ', msg)
            });

        }
        $scope.getData();
        setInterval(function () {
            $scope.getData();
        }, 60000);
        
    }]);